<?php

add_filter('gform_init_scripts_footer', '__return_true');

function gform_cdata_open($content = '') {
  $content = 'document.addEventListener( "DOMContentLoaded", function() { ';
  return $content;
}

add_filter('gform_cdata_open', __NAMESPACE__ . '\\gform_cdata_open');

function gform_cdata_close( $content = '' ) {
  $content = ' }, false );';
  return $content;
}

add_filter('gform_cdata_close', __NAMESPACE__ . '\\gform_cdata_close');

//END Force Gravity Forms to init scripts in the footer

add_action('after_setup_theme', 'register_mystirling_nav_areas');
function register_mystirling_nav_areas() {
    register_nav_menus(array(
        'header_nav' => 'Site Navigation Main',
        'footer_nav' => 'Site Navigation Footer',
        'subsite_nav' => 'Subsite Navigation'
    ));
}

if (function_exists('acf_add_options_page')) {
  acf_add_options_page(array(
    'page_title'  => 'Theme Global Options',
    'menu_title'  => 'MyStirling Info',
    'menu_slug'  => 'theme-general-settings',
    'position' => 30,
    'capability' => 'edit_posts',
    'redirect'   => false,
    'icon_url'   => 'https://s3-ap-southeast-2.amazonaws.com/loaded-public/loaded-static/img/theme-edit.png',
  ));
}

