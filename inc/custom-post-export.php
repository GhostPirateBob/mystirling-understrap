<?php 
function cptui_register_my_cpts() {

/**
 * Post Type: Insights.
 */

$labels = array(
  "name" => __( "Insights", "understrap" ),
  "singular_name" => __( "Insight", "understrap" ),
);

$args = array(
  "label" => __( "Insights", "understrap" ),
  "labels" => $labels,
  "description" => "",
  "public" => true,
  "publicly_queryable" => true,
  "show_ui" => true,
  "delete_with_user" => false,
  "show_in_rest" => true,
  "rest_base" => "",
  "rest_controller_class" => "WP_REST_Posts_Controller",
  "has_archive" => "insights",
  "show_in_menu" => true,
  "show_in_nav_menus" => true,
  "exclude_from_search" => false,
  "capability_type" => "post",
  "map_meta_cap" => true,
  "hierarchical" => false,
  "rewrite" => array( "slug" => "insights", "with_front" => true ),
  "query_var" => true,
  "menu_icon" => "dashicons-analytics",
  "supports" => array( "title", "editor", "thumbnail" ),
);

register_post_type( "insights", $args );

/**
 * Post Type: Promotions.
 */

$labels = array(
  "name" => __( "Promotions", "understrap" ),
  "singular_name" => __( "Promotion", "understrap" ),
);

$args = array(
  "label" => __( "Promotions", "understrap" ),
  "labels" => $labels,
  "description" => "",
  "public" => true,
  "publicly_queryable" => true,
  "show_ui" => true,
  "delete_with_user" => false,
  "show_in_rest" => true,
  "rest_base" => "",
  "rest_controller_class" => "WP_REST_Posts_Controller",
  "has_archive" => false,
  "show_in_menu" => true,
  "show_in_nav_menus" => true,
  "exclude_from_search" => false,
  "capability_type" => "post",
  "map_meta_cap" => true,
  "hierarchical" => false,
  "rewrite" => array( "slug" => "promotions", "with_front" => true ),
  "query_var" => true,
  "menu_icon" => "dashicons-awards",
  "supports" => array( "title", "editor", "thumbnail" ),
);

register_post_type( "promotions", $args );

/**
 * Post Type: Developments.
 */

$labels = array(
  "name" => __( "Developments", "understrap" ),
  "singular_name" => __( "Development", "understrap" ),
);

$args = array(
  "label" => __( "Developments", "understrap" ),
  "labels" => $labels,
  "description" => "",
  "public" => true,
  "publicly_queryable" => true,
  "show_ui" => true,
  "delete_with_user" => false,
  "show_in_rest" => true,
  "rest_base" => "",
  "rest_controller_class" => "WP_REST_Posts_Controller",
  "has_archive" => "developments",
  "show_in_menu" => true,
  "show_in_nav_menus" => true,
  "exclude_from_search" => false,
  "capability_type" => "post",
  "map_meta_cap" => true,
  "hierarchical" => false,
  "rewrite" => array( "slug" => "developments", "with_front" => true ),
  "query_var" => true,
  "menu_position" => 19,
  "menu_icon" => "dashicons-building",
  "supports" => array( "title", "editor", "thumbnail" ),
);

register_post_type( "developments", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );
