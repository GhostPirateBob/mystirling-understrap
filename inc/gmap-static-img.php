<?php
require_once(__DIR__ . '/../vendor/autoload.php');

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

// exit if accessed directly
if( ! defined( 'ABSPATH' ) ) exit;

// Generate static google map for locations

add_action( 'save_post', 'update_gmap_static_img_save_post', 10, 5 );

function update_gmap_static_img_save_post( $post_ID, $post ) {
  if ( $post->post_type !== 'developments' ){
    return;
  }
  if ( $post->post_type === 'developments' ) {
    if ( get_field( "google_map_image", $post_ID ) === false ||
          strlen( get_field("google_map_image", $post_ID) ) < 5 ||
          get_field( "google_map_delete_cached_image" ) !== false &&  get_field( "google_map_delete_cached_image" ) === 'yes' ) {
      $theme_dir = get_template_directory_uri();
      $site_url = get_site_url();
      $location_gmap = get_field('google_map_google_map_location', $post_ID);
      $location_image = get_field('google_map_image', $post_ID);
      $location_uuid = get_field( 'google_map_location_uuid', $post_ID );
      if ( get_field('google_map_marker_image') ) {
        $location_marker_image = get_field('google_map_marker_image');
        if ( is_array( $location_marker_image ) && array_key_exists('url', $location_marker_image ) ) {
          $location_marker_image = $location_marker_image['url'];
        }
      }
      if ( !get_field('google_map_marker_image') ) {
        $location_marker_image = 'https://s3-ap-southeast-2.amazonaws.com/loaded-public/loaded-static/img/mystirling-map-marker.png';
      }
      $lat = strval($location_gmap['lat']);
      $lng = strval($location_gmap['lng']);
      $curl = 'https://maps.googleapis.com/maps/api/staticmap?markers=icon:'.$location_marker_image.'%7C'.$lat.','.$lng.'&maptype=roadmap&&center='.$lat.','.$lng.'&zoom=15&format=png&zoom=15&maptype=roadmap&style=element:geometry%7Ccolor:0xf5f5f5%7Csaturation:-100%7Cweight:1.5&style=element:labels.icon%7Cvisibility:off&style=element:labels.text.fill%7Ccolor:0x616161&style=element:labels.text.stroke%7Ccolor:0xf5f5f5&style=feature:administrative%7Celement:geometry%7Cvisibility:off&style=feature:administrative.land_parcel%7Cvisibility:off&style=feature:administrative.land_parcel%7Celement:labels.text.fill%7Ccolor:0xbdbdbd&style=feature:administrative.neighborhood%7Cvisibility:off&style=feature:landscape%7Celement:geometry%7Ccolor:0xecf0f0&style=feature:poi%7Cvisibility:off&style=feature:poi%7Celement:geometry%7Ccolor:0xeeeeee&style=feature:poi%7Celement:labels.text%7Cvisibility:off&style=feature:poi%7Celement:labels.text.fill%7Ccolor:0x757575&style=feature:poi.park%7Celement:geometry%7Ccolor:0xf4f4f4&style=feature:poi.park%7Celement:labels.text.fill%7Ccolor:0x9e9e9e&style=feature:road%7Celement:geometry%7Ccolor:0xffffff%7Cweight:1&style=feature:road%7Celement:labels%7Cvisibility:off&style=feature:road%7Celement:labels.icon%7Cvisibility:off&style=feature:road.arterial%7Celement:labels.text.fill%7Ccolor:0x757575&style=feature:road.highway%7Celement:geometry%7Ccolor:0xd0d0d0%7Cweight:2&style=feature:road.highway%7Celement:labels.text.fill%7Ccolor:0x616161&style=feature:road.local%7Celement:labels.text.fill%7Ccolor:0x9e9e9e&style=feature:transit%7Cvisibility:off&style=feature:transit.line%7Celement:geometry%7Ccolor:0xe5e5e5&style=feature:transit.station%7Celement:geometry%7Ccolor:0xeeeeee&style=feature:water%7Celement:geometry%7Ccolor:0xc8c8c8&style=feature:water%7Celement:labels.text%7Cvisibility:off&style=feature:water%7Celement:labels.text.fill%7Ccolor:0x9e9e9e&size=754x452&key=AIzaSyBw-Ld2uyp74D05cs6QOK3CVpnKcjxJ1e0';
      $template_dir  = get_template_directory();
      $root_dir = explode('/themes/', $template_dir);
      if ( !file_exists($root_dir[0].'/uploads/google-images/') ) {
        mkdir( $root_dir[0].'/uploads/google-images/', 0767, true );
      }
      $upload_dir = $root_dir[0].'/uploads/google-images/';
      $fileUrl = $curl;
      $saveTo = $upload_dir.$post_ID.'.png';
      if ( file_exists( $saveTo ) && get_field( "google_map_delete_cached_image" ) && get_field( "google_map_delete_cached_image" ) === 'yes' ) {
        fclose($saveTo);
        unlink($saveTo);
      }
      $fp = fopen($saveTo, 'w+');
      if ($fp === false) {
        throw new Exception('Could not open: ' . $saveTo);
      }
      $ch = curl_init($fileUrl);
      curl_setopt($ch, CURLOPT_FILE, $fp);
      curl_setopt($ch, CURLOPT_TIMEOUT, 30);
      curl_exec($ch);
      if (curl_errno($ch)) {
          throw new Exception(curl_error($ch));
          update_field('google_map_image', curl_error($ch), $post_ID);
      } else {
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $final_url = $site_url.'/wp-content/uploads/google-images/'.$post_ID.'.png';
        update_field('google_map_image', $final_url, $post_ID);
      }
      if ( get_field( 'google_map_location_uuid', $post_ID ) && strlen( get_field( 'google_map_location_uuid', $post_ID ) ) < 5 ) {
        $uuid4 = Uuid::uuid4();
        update_field( 'google_map_location_uuid', $uuid4, $post_ID );
      }
      update_field( "google_map_delete_cached_image", 'no', $post_ID );
      return;
    }
  }
}