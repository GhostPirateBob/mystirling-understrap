<?php
/**
 * Understrap enqueue scripts
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}

if ( ! function_exists( 'understrap_scripts' ) ) {
  /**
   * Load theme's JavaScript and CSS sources.
   */
  function understrap_scripts() {
    wp_deregister_script('sack');
    wp_deregister_script('wp-block-library');
    wp_deregister_style('wp-block-library');
    if ( !is_admin() ) {
      wp_deregister_script('wp-block-library');
      wp_deregister_style('wp-block-library');
      $style_theme = get_stylesheet_directory() . '/css/theme.min.css';
      $style_theme_uri = get_stylesheet_directory_uri() . '/css/theme.min.css';
      if ( file_exists( $style_theme ) ) {
        wp_enqueue_style( 'understrap-styles', $style_theme_uri, array(), filemtime( $style_theme ) );
      }

      $script_jquery = get_stylesheet_directory() . '/js/jquery-blank.min.js';
      $script_jquery_uri = get_stylesheet_directory_uri() . '/js/jquery-blank.min.js';

      if ( file_exists( $script_jquery ) ) {
        wp_deregister_script('jquery');
        wp_deregister_script('jquery-migrate');
        wp_enqueue_script( 'jquery',  $script_jquery_uri, array(), '3.4.1', false );
      }

      $script_theme_header = get_stylesheet_directory() . '/js/theme-header.js';
      $script_theme_header_uri = get_stylesheet_directory_uri() . '/js/theme-header.js';
      if ( file_exists( $script_theme_header ) ) {
        wp_enqueue_script( 'theme-header',  $script_theme_header_uri, array(), filemtime( $script_theme_header ), false );
      }

      $script_theme = get_stylesheet_directory() . '/js/theme.js';
      $script_theme_uri = get_stylesheet_directory_uri() . '/js/theme.js';
      if ( file_exists( $script_theme ) ) {
        wp_enqueue_script( 'theme-scripts', $script_theme_uri, array('jquery'), filemtime( $script_theme ), true );
      }
    }
  }
} // endif function_exists( 'understrap_scripts' ).

add_action( 'wp_enqueue_scripts', 'understrap_scripts' );
