<?php 
// Single Apartment
$siteURL    = get_bloginfo('url');
$themeURL   = get_stylesheet_directory_uri();
$siteTitle  = get_bloginfo('Title');
$themePath  = get_stylesheet_directory();
$imgPath    = $themeURL . '/img/apt-preview-0' . rand( 1, 6 ) . '.png';
?> 

  <div class="col-48 col-md-24 col-xl-16 col-featured-apartment gutters mb-4">
    <div class="card dp-00 b-0">
      <img class="card-img-top" src="<?php echo $imgPath; ?>" alt="<?php the_title(); ?>" />
      <div class="card-body">
        <h5 class="card-title fw-400"><strong> Barque, </strong> North Coogee </h5>
        <?php if ( get_field('number') ) { ?>
        <p class="card-text mb-0"> Apartment <?php the_field('number'); ?> </p>
        <?php } ?>
        <?php if ( get_field('beds') && get_field('beds') ) { ?>
        <p class="card-text mb-0"> 
          <?php the_field( 'beds' ); ?> bed &#65291; 
          <?php the_field( 'baths' ); ?> bath
          <?php if ( get_field('carbays') ) { ?>
            &#65291; <?php the_field( 'carbays' ); ?> car
          <?php } ?>
        </p>
        <?php } ?>
        <?php if ( get_field('price') ) { ?>
        <h5 class="card-text mb-0 mt-1"> <?php
          $price = get_field('price');
          setlocale(LC_MONETARY,"en_AU");
          echo '&dollar;' . number_format($price); ?>
        </h5>
        <?php } else { ?>
        <h5 class="card-text mb-0 mt-1"> <?php
          $price = rand(250,4500) * 1000;
          setlocale(LC_MONETARY,"en_AU");
          echo '&dollar;' . number_format($price); ?>
        </h5>
        <?php } ?>
      </div>
      <div class="card-footer">
        <a name="viewApartment" class="btn btn-link btn-arrow-right btn-arrow-right-link" href="<?php the_permalink(); ?>" role="button">
          <span class="btn-arrow-text"> View Apartment </span>
          <?php echo file_get_contents($themePath . '/img/arrow-right-dark.svg'); ?> 
        </a>
      </div>
    </div>
  </div>
<?php /*
<?php the_field( 'number' ); ?>
<?php the_field( 'level' ); ?>
<?php the_field( 'status' ); ?>
<?php the_field( 'type' ); ?>
<?php the_field( 'plan' ); ?>
<?php the_field( 'beds' ); ?>
<?php the_field( 'baths' ); ?>
<?php the_field( 'carbays' ); ?>
<?php the_field( 'mc_bay' ); ?>
<?php the_field( 'internal_architectural_area' ); ?>
<?php the_field( 'balcony_architectural_area' ); ?>
<?php the_field( 'internal_strata_area' ); ?>
<?php the_field( 'balcony_strata_area' ); ?>
<?php the_field( 'total_area' ); ?>
<?php the_field( 'total_outdoor_area' ); ?>
<?php the_field( 'store' ); ?>
<?php the_field( 'aspect' ); ?>
<?php $floorplate_pdf = get_field( 'floorplate_pdf' ); ?>
<?php if ( $floorplate_pdf ) { ?>
	<a href="<?php echo $floorplate_pdf['url']; ?>"><?php echo $floorplate_pdf['filename']; ?></a>
<?php } ?>
<?php the_field( 'floorplate_url' ); ?>
<?php the_field( 'floorplate_pdf_url' ); ?>
<?php // The unique_id field type is not supported in this version of the plugin. ?>
<?php // ?>
*/ ?>

