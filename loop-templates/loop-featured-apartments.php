<?php 
// Single Apartment
$siteURL    = get_bloginfo('url');
$themeURL   = get_stylesheet_directory_uri();
$siteTitle  = get_bloginfo('Title');
$themePath = get_stylesheet_directory();
if ( $apartmentID ) {
  foreach ($apartmentObject as $apartment) {
    if ( intval($apartmentID) === intval($apartment['_id']) ) {
      $apartmentAltID = $apartment['id'];
    }
  }
}
?> 

<div class="col-48 col-featured-apartment gutters">
  <img class="featured-apartment-img img-fluid w-100" src="<?php echo $apartmentObject[$apartmentID]['primary_image_sm'];?>" />
  <div class="card dp-00 b-0">
    <div class="card-body">
      <h5 class="card-title fw-400 mb-0"><strong> <?php echo $apartmentObject[$apartmentID]['address_building_name'];?>, </strong> <?php echo $apartmentObject[$apartmentID]['address_suburb_or_town'];?> </h5>
      
      <p class="card-text mb-0"> Apartment <?php echo $apartmentObject[$apartmentID]['address_unit_number'];?> </p>
      
     
      <p class="card-text mb-0"> 
      <?php echo $apartmentObject[$apartmentID]['attributes_bedrooms'];?> bed &#65291; 
      <?php echo $apartmentObject[$apartmentID]['attributes_bathrooms'];?> bath
      <?php if ( $apartmentObject[$apartmentID]['attributes_garages'] ) { ?>
              &#65291; <?php echo $apartmentObject[$apartmentID]['attributes_garages'];?> carbay
            <?php } ?>
      </p>
    
    
      <h5 class="card-text mb-0 mt-1"> <?php
if ( $apartmentObject[$apartmentID]['price_match']) {
  setlocale(LC_MONETARY,"en_AU");
  echo '&dollar;' . number_format($apartmentObject[$apartmentID]['price_match']);
}
?>
      </h5>
    
    </div>
    <div class="card-footer bg-white b-0">
      <a name="viewApartment" class="btn btn-link btn-arrow-right btn-arrow-right-dark btn-arrow-right-hover-primary" href="<?php echo $siteURL . '/apartment/?apartment=' . $apartmentObject[$apartmentID]['_id'] ;?>" role="button">
        <span class="btn-arrow-text fw-500"> View Apartment </span>
        <?php echo file_get_contents($themePath . '/img/arrow-right-dark.svg'); ?> 
      </a>
    </div>
  </div>
</div>
