<?php 

// Single Development Loop Item

$logo_black = get_field( 'logo_black' );
$slider_image = get_field( 'slider_image' );
$address_two = get_field ( 'address_two');
$status_array = get_field( 'status' );

$description = get_field( 'description' );
$website_link = get_field( 'website_link' );
$apartments_link = get_field( 'apartments_link' );

$siteURL = get_bloginfo('url');
$themeURL  = get_stylesheet_directory_uri();
$siteTitle = get_bloginfo('Title');
$themePath = get_stylesheet_directory();
$banner_image = get_field ( 'banner_image', 700);
$banner_content = get_field ('banner_content' , 700);

?> 

<div class="row development_row mb-7 gutters">
  <img src="<?php echo $slider_image['url']; ?>" class="img-fit">
  <div class="row development_row_outer w-100 h-100">
    <div class="container container-inner d-flex align-items-center">
      <div class="stripe "></div>
      <div class="row development_row_inner">
        <div class="col-48 development_col">
          <div class="development_content dp-0" data-aos="fade-up">
            <img src="<?php echo $logo_black['url']?>" class="logo mb-4">
            <h4 class="entry-title justify-self-center align-self-center text-primary mb-0 "><?php the_title();?></h4>
            <p class="mb-1"><?php echo $address_two;?></p>
            <ul class="list-unstyled d-flex flex-column flex-lg-row mb-4"> 
            
              <?php if ( $status_array ) {
                foreach ( $status_array as $status_item ) { ?>
                <li class="pr-2"><?php echo $status_item['label'];?> </li> 
              <?php }
              } ?> 
            </ul>
            <p class="text-description"><?php echo $description;?></p>
            <a class="btn btn-link btn-arrow-right btn-arrow-right-dark btn-arrow-right-hover-primary mt-auto mb-2 mr-4" type="link"
              href="<?php echo $website_link['url']; ?>" <?php echo $website_link['target']; ?>>
              <span class="btn-arrow-text text-uppercase fw-500">
                <?php echo $website_link['text']; ?>
                <?php echo file_get_contents($themePath . '/img/arrow-right-dark.svg'); ?>
              </span>
            </a>
            <a class="btn btn-link btn-arrow-right btn-arrow-right-dark btn-arrow-right-hover-primary mb-2 " type="link"
              href="<?php echo $apartments_link['url']; ?>" <?php echo $apartments_link['target']; ?>>
              <span class="btn-arrow-text text-uppercase fw-500">
                <?php echo $apartments_link['text']; ?>
                <?php echo file_get_contents($themePath . '/img/arrow-right-dark.svg'); ?>
              </span>
            </a>
          </div>
        </div>
       
      </div>
    </div>
  </div>
</div>
