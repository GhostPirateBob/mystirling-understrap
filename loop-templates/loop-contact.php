<?php

$title                    = get_the_title();
$permalink                = get_the_permalink();

$logo_black               = get_field('logo_black');
$our_developments_image   = get_field('our_developments_image');
$short_description        = get_field('short_description');
$website_link             = get_field('website_link');
$address_one              = get_field('address_one');
$address_two              = get_field('address_two');
$short_name               = get_field('short_name');
$google_map_image         = get_field('google_map_image');
$opening_title            = get_field('opening_title');
$contact_agent_name       = get_field('contact_agent_name');
$contact_agent_number     = get_field('contact_agent_number');
$google_map_link          = get_field('google_map_link_directions');
$rex_building_id          = get_field('rex_building_id');
?>
<div class="row row-contact-development py-3 py-lg-5" data-aos="fade-up">
<div class="col-48 col-lg-4"></div>
  <div class="col-48 col-lg-10 py-2 gutters">
    <img src="<?php echo $logo_black['url'] ?>" class="logo mb-3 mb-lg-5">
    <h5 class="text-primary  mb-4" style="font-size:1.375rem;"> <?php echo $title;?> </h5>
    <h4 class="text-dark mb-0"> <?php echo $contact_agent_name;?> </h4>
    <h4 class="text-dark mb-4"> <?php echo $contact_agent_number;?> </h4>
    <h5 class="font-weight-normal mb-4"> <?php echo $address_one; ?> <br class="d-none d-md-block"> <?php echo $address_two; ?> </h5>
    <h5 class="font-weight-normal mb-3"> <?php echo $opening_title; ?> </h5>
      <?php if ( have_rows( 'open_hours' ) ) { ?>
        <?php while ( have_rows( 'open_hours' ) ) { 
          the_row(); ?>
          <h5 class="font-weight-normal opening-hours mb-0 d-flex" >
            <span class="pr-2"><?php the_sub_field( 'text' ); ?></span>
            <span><?php the_sub_field( 'start_time' ); ?> 
          <?php if ( get_sub_field('end_time') ) { ?>
            - <?php the_sub_field( 'end_time' ); ?>
          <?php } ?></span>
          </h5>
        <?php }
            } ?>
    <button type="button" class="btn btn-outline-dark p-3 mt-5 mb-4 fw-500" data-toggle="modal" data-target="#modalCenter" data-project-title="<?php echo $title; ?>" data-project-id="<?php echo $rex_building_id; ?>">Request Viewing</button>
  </div>
  <div class="col-48 col-lg-4"></div>
  <div class="col-48 col-lg-26 gutters">
    <img src="<?php echo $our_developments_image['url']; ?>" class="img-fluid mb-3 d-block w-100" />
    <img src="<?php echo $google_map_image; ?>" class="img-fluid d-block w-100 mb-3" />
    <a class="" href="<?php echo $google_map_link;?>" target="_blank" rel="nofollow"> Get Directions on Google Maps </a>
  </div>
  <div class="col-48 col-lg-4"></div>
</div>

