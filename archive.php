<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

?>

<div class="wrapper" id="archive-wrapper">
  <div class="container-fluid" id="content" tabindex="-1">
    <div class="row">
      <div class="col content-area" id="primary">
        <div class="entry-content">
        <?php if ( have_posts() ) : ?>
          <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
          <?php endwhile; // end of the loop. ?>
        <?php endif; ?>
        </div>
      </div>
    </div> 
  </div>
</div><?php // need this extra closing tag ?>
</div>
<?php get_footer(); ?>
