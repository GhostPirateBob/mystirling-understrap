document.documentElement.classList.add( 'js' );
if ( ( navigator.userAgent.toLowerCase()
    .indexOf( 'webkit' ) > -1 ) ) {
  document.documentElement.classList.add( 'webkit' );
}
if ( window.navigator.userAgent.indexOf( "Trident/7.0" ) > -1 ) {
  document.documentElement.classList.add( 'msie' );
}
/* Prevent Safari opening links when viewing as a Mobile App */
( function( a, b, c ) {
  if ( c in b && b[ c ] ) {
    var d, e = a.location,
      f = /^(a|html)$/i;
    a.addEventListener( "click", function( a ) {
      d = a.target;
      while ( !f.test( d.nodeName ) ) d = d.parentNode;
      "href" in d && ( d.href.indexOf( "http" ) || ~d.href.indexOf( e.host ) ) && ( a.preventDefault(), e.href = d
        .href )
    }, !1 )
  }
} )( document, window.navigator, "standalone" );

/**
 * @license
 * Lodash (Custom Build) lodash.com/license | Underscore.js 1.8.3 underscorejs.org/LICENSE
 * Build: `lodash include="throttle,debounce"`
 */
;(function(){function t(){}function e(t,e,o){function r(e){var n=p,o=s;return p=s=u,g=e,y=t.apply(o,n)}function f(t){var n=t-m;return t-=g,m===u||n>=e||0>n||v&&t>=b}function a(){var t=h();if(f(t))return c(t);var n,o=setTimeout;n=t-g,t=e-(t-m),n=v?O(t,b-n):t,d=o(a,n)}function c(t){return d=u,T&&p?r(t):(p=s=u,y)}function l(){var t=h(),n=f(t);if(p=arguments,s=this,m=t,n){if(d===u)return g=t=m,d=setTimeout(a,e),j?r(t):y;if(v)return d=setTimeout(a,e),r(m)}return d===u&&(d=setTimeout(a,e)),y}var p,s,b,y,d,m,g=0,j=false,v=false,T=true;
if(typeof t!="function")throw new TypeError("Expected a function");return e=i(e)||0,n(o)&&(j=!!o.leading,b=(v="maxWait"in o)?x(i(o.maxWait)||0,e):b,T="trailing"in o?!!o.trailing:T),l.cancel=function(){d!==u&&clearTimeout(d),g=0,p=m=s=d=u},l.flush=function(){return d===u?y:c(h())},l}function n(t){var e=typeof t;return null!=t&&("object"==e||"function"==e)}function o(t){return null!=t&&typeof t=="object"}function r(t){var e;if(!(e=typeof t=="symbol")&&(e=o(t))){if(null==t)t=t===u?"[object Undefined]":"[object Null]";else if(v&&v in Object(t)){
e=g.call(t,v);var n=t[v];try{t[v]=u;var r=true}catch(t){}var i=j.call(t);r&&(e?t[v]=n:delete t[v]),t=i}else t=j.call(t);e="[object Symbol]"==t}return e}function i(t){if(typeof t=="number")return t;if(r(t))return f;if(n(t)&&(t=typeof t.valueOf=="function"?t.valueOf():t,t=n(t)?t+"":t),typeof t!="string")return 0===t?t:+t;t=t.replace(a,"");var e=l.test(t);return e||p.test(t)?s(t.slice(2),e?2:8):c.test(t)?f:+t}var u,f=NaN,a=/^\s+|\s+$/g,c=/^[-+]0x[0-9a-f]+$/i,l=/^0b[01]+$/i,p=/^0o[0-7]+$/i,s=parseInt,b=typeof self=="object"&&self&&self.Object===Object&&self,y=typeof global=="object"&&global&&global.Object===Object&&global||b||Function("return this")(),d=(b=typeof exports=="object"&&exports&&!exports.nodeType&&exports)&&typeof module=="object"&&module&&!module.nodeType&&module,m=Object.prototype,g=m.hasOwnProperty,j=m.toString,v=(m=y.Symbol)?m.toStringTag:u,x=Math.max,O=Math.min,h=function(){
return y.Date.now()};t.debounce=e,t.throttle=function(t,o,r){var i=true,u=true;if(typeof t!="function")throw new TypeError("Expected a function");return n(r)&&(i="leading"in r?!!r.leading:i,u="trailing"in r?!!r.trailing:u),e(t,o,{leading:i,maxWait:o,trailing:u})},t.isObject=n,t.isObjectLike=o,t.isSymbol=r,t.now=h,t.toNumber=i,t.VERSION="4.17.5",typeof define=="function"&&typeof define.amd=="object"&&define.amd?(y._=t, define(function(){return t})):d?((d.exports=t)._=t,b._=t):y._=t}).call(this);
  var pluginName = "ldSidenav";

  function debounceLead( callback, offset ) {
    var baseTime = 0;
    return function() {
      var args = [],
        len = arguments.length;

      while ( len-- ) args[ len ] = arguments[ len ];

      var currentTime = Date.now();

      if ( baseTime + offset <= currentTime ) {
        callback.apply( void 0, args );
        baseTime = currentTime;
      }
      else {
        baseTime = currentTime;
      }
    };
  };

  function debounceTail( callback, offset ) {
    var timeoutFunc = null;
    return function() {
      var args = [],
        len = arguments.length;

      while ( len-- ) args[ len ] = arguments[ len ];

      clearTimeout( timeoutFunc );
      timeoutFunc = setTimeout( function() {
        callback.apply( void 0, args );
      }, offset );
    };
  }; // call the correct debounce function


  function uniqid( ) {
    var a = "";
    var b = false;
    var c = Date.now() / 1000;
    var d = c.toString( 16 )
      .split( "." )
      .join( "" );
    while ( d.length < 14 ) {
      d += "0";
    }
    var e = "";
    if ( b ) {
      e = ".";
      var f = Math.round( Math.random() * 100000000 );
      e += f;
    }
    return a + d + e;
  };

  jQuery.fn[ pluginName ] = function( options ) {
    var currentID = uniqid();
    // Default settings 
    var ldSidenavConfig = jQuery.extend( true, {
      attr: "ldsidebar",
      top: 0,
      gap: 0,
      zIndex: 1200,
      htmlOverflowX: "hidden",
      bodyOverflowX: "hidden",
      pageOverflowX: "hidden",
      htmlOverflowY: "initial",
      bodyOverflowY: "initial",
      pageOverflowY: "auto",
      uniqueID: currentID,
      quitter: jQuery( "<a>" )
        .attr( 'data-quitter-id', currentID )
        .attr( 'class', 'ldSidenavQuitter' )
        .attr( 'id', 'quitter-' + currentID )
        .css( 'visibility', 'hidden' )
        .css( 'opacity', '0' )
        .css( 'height', '0' )
        .css( 'width', '0' )
        .css( 'position', 'absolute' )
        .css( 'left', '-9999px' )
        .css( 'overflow', 'hidden' )
        .appendTo( "body" ),
      sidebar: {
        width: 320
      },

      animation: {
        duration: 1800,
        easing: "easeOutPower3"
      },

      events: {
        on: {
          animation: {
            open: function() {},
            close: function() {},
            both: function() {}
          }
        },
        callbacks: {
          animation: {
            open: function() {},
            close: function() {},
            both: function() {},
            freezePage: true
          }
        }
      },

      mask: {
        display: true,
        css: {
          backgroundColor: "rgba(0, 0, 0, 0.3)",
        }
      }
    }, options );

    // Keep chainability 
    return this.each( function() {
      var ldStyle, pvtMaskStyle, maskStyle,
        attr = "data-" + ldSidenavConfig.attr,

        // Set anything else than "opened" to "closed" 
        init = ( "opened" === ldSidenavConfig.init ) ? "opened" : "closed",

        // Set the overflow setting to initial 
        htmlOverflowX = ldSidenavConfig.htmlOverflowX ? ldSidenavConfig.htmlOverflowX : jQuery( "html" )
        .css( "overflow-x" ),
        bodyOverflowX = ldSidenavConfig.bodyOverflowX ? ldSidenavConfig.bodyOverflowX : jQuery( "body" )
        .css( "overflow-x" ),
        pageOverflowX = ldSidenavConfig.pageOverflowX ? ldSidenavConfig.pageOverflowX : jQuery( "#page" )
        .css( "overflow-x" ),
        htmlOverflowY = ldSidenavConfig.htmlOverflowY ? ldSidenavConfig.htmlOverflowY : jQuery( "html" )
        .css( "overflow-y" ),
        bodyOverflowY = ldSidenavConfig.bodyOverflowY ? ldSidenavConfig.bodyOverflowY : jQuery( "body" )
        .css( "overflow-y" ),
        pageOverflowY = ldSidenavConfig.pageOverflowY ? ldSidenavConfig.pageOverflowY : jQuery( "#page" )
        .css( "overflow-y" ),
        // Set anything else than "left" to "right" 
        align = ( "left" === ldSidenavConfig.align ) ? "left" : "right",
        duration = ldSidenavConfig.animation.duration,
        durationInSeconds = ldSidenavConfig.animation.duration / 1000,
        gsapDurationInSeconds = 0.75,
        gsapDurationInSecondsLg = 1.25,
        easing = ldSidenavConfig.animation.easing,
        animation = {},
        gsapAnimation = {},
        // Set anything else then true to false 
        scrollCfg = ( true === ldSidenavConfig.events.callbacks.animation.freezePage ) ?
        true :
        false,
        freezePage = function() {
          jQuery( "html" )
            .css( "overflow-x", "hidden" )
            .css( "overflow-y", "auto" );
          jQuery( "body" )
            .css( "overflow-x", "hidden" )
            .css( "overflow-y", "auto" );
          if ( jQuery( '#page' )
            .length > 0 ) {
            jQuery( "#page" )
              .css( "overflow-y", "hidden" )
              .css( "overflow-x", "hidden" );
            //   .css('margin-right', '-10px !important')
            //   .css('width', 'auto !important')
            //   .css('max-width', 'auto !important');
            // jQuery( "#navbar-right-main" )
            //   .css('right', '10px !important');
          }
        },
        unfreezePage = function() {
          jQuery( "html" )
            .css( "overflow-x", htmlOverflowX )
            .css( "overflow-y", htmlOverflowY );
          jQuery( "body" )
            .css( "overflow-x", bodyOverflowX )
            .css( "overflow-y", bodyOverflowY );
          if ( jQuery( '#page' )
            .length > 0 ) {
            jQuery( "#page" )
              .css( "overflow-x", pageOverflowX )
              .css( "overflow-y", pageOverflowY );
            }
        },

        // Sidenav helpers 
        ldSidenav = jQuery( this ),
        setSidenavWidth = function( width ) {
          // Calculate sidebar width 
          if ( ldSidenav.attr( 'id' ) === 'sidebar-right-nav' || ldSidenav.attr( 'id' ) ===
            'sidebar-right-enquire' ) {
            return jQuery( window )
              .width();
          }
          else {
            if ( width < ( ldSidenavConfig.sidebar.width + ldSidenavConfig.gap ) ) {
              return width - ldSidenavConfig.gap;
            }
            else {
              return ldSidenavConfig.sidebar.width;
            }
          }
        },
        sidebarStatus = function() {
          // Check if the sidebar attribute is set to "opened" or "closed" 
          return ldSidenav.attr( attr );
        },
        changeSidenavStatus = function( status ) {
          ldSidenav.attr( attr, status );
        },
        ldSidenavMask = jQuery( '#ldSidenavMask' )
        .length ? jQuery( '#ldSidenavMask' ) : jQuery( "<div>" )
        .attr( attr, "mask" )
        .attr( 'class', 'ldSidenavMask' )
        .attr( 'id', 'ldSidenavMask' ),
        createMask = function() {
          // Create mask 
          if ( jQuery( "#ldSidenavMask" )
            .length === 0 ) {
            if ( jQuery( "#page" )
              .length === 1 ) {
              ldSidenavMask.prependTo( "#page" )
                .css( maskStyle );
            }
            else {
              ldSidenavMask.appendTo( "body" )
                .css( maskStyle );
            }
          }
        },
        showMask = function() {
          ldSidenavMask.fadeIn( 450 );
        },
        hideMask = function() {
          ldSidenavMask.fadeOut( 300 );
        },

        ldSidenavTrigger = jQuery( ldSidenavConfig.selectors.trigger ),
        ldSidenavClose = ldSidenavConfig.quitter,

        width = jQuery( window )
        .width(),

        // Other functions that must be run along the animation 
        events = {

          // Events triggered with the animations 
          on: {
            animation: {
              open: function() {
                console.log(ldSidenav.attr('id'));
                jQuery( '.ldSidenavMask' )
                  .each( function( index, element ) {
                    dataSidenavID = jQuery( this )
                      .attr( 'data-sidenav-id' );
                  } );
                showMask();
                changeSidenavStatus( "opened" );

                ldSidenavConfig.events.on.animation.open();
              },
              close: function() {
                hideMask();
                changeSidenavStatus( "closed" );

                ldSidenavConfig.events.on.animation.close();
              },
              both: function() {
                ldSidenavConfig.events.on.animation.both();
              }
            }
          },

          // Events triggered after the animations 
          callbacks: {
            animation: {
              open: function() {
                if ( scrollCfg ) {
                  freezePage();
                }
                ldSidenavConfig.events.callbacks.animation.open();
              },
              close: function() {
                if ( scrollCfg ) {
                  unfreezePage();
                }

                ldSidenavConfig.events.callbacks.animation.close();
              },
              both: function() {
                ldSidenavConfig.events.callbacks.animation.both();
              }
            }
          }
        },

        // Create animations 
        animateOpen = function() {

          var buttonID = ldSidenav.attr( 'id' ) + '-button';
          if ( jQuery( '#' + buttonID )
            .hasClass( 'tcon' ) ) {
            jQuery( '#' + buttonID )
              .addClass( 'tcon-transform' )
          }

          if ( buttonID === 'sidebar-left-enquire-button' ) {
            jQuery( '.enquire-btn-chevron-right' )
              .addClass( 'show' );
          }

          var callbacks = function() {
            events.callbacks.animation.open();
            events.callbacks.animation.both();
          };

          // Define the animation 
          animation[ align ] = 0;
          var gsapOpenComplete = function() {
            events.callbacks.animation.open();
            events.callbacks.animation.both();
          };

          if ( align === 'left') {
            gsapAnimation = {
              left: 0,
              onComplete: gsapOpenComplete
            };
          }

          if ( align !== 'left' ) {
            gsapAnimation = {
              right: 0,
              onComplete: gsapOpenComplete
            };
          }

          if ( ldSidenav.width() > 668 ) {
            TweenLite.to( ldSidenav, gsapDurationInSecondsLg, gsapAnimation );
          }
          else {
            TweenLite.to( ldSidenav, gsapDurationInSeconds, gsapAnimation );
          }

          // ldSidenav.animate( animation, duration, easing, callbacks ); 

          events.on.animation.open();
          events.on.animation.both();
        },
        animateClose = function() {
          var buttonID = ldSidenav.attr( 'id' ) + '-button';

          if ( jQuery( '#' + buttonID )
            .hasClass( 'tcon' ) ) {
            jQuery( '#' + buttonID )
              .removeClass( 'tcon-transform' )
          }

          if ( buttonID === 'sidebar-left-enquire-button' ) {
            jQuery( '.enquire-btn-chevron-right' )
              .removeClass( 'show' );
          }

          if ( ldSidenav[ 0 ][ 'id' ] === 'sidebar-right-nav' && jQuery( window ).width() <= 667 ||
                ldSidenav[ 0 ][ 'id' ] === 'sidebar-right-enquire' && jQuery( window ).width() <= 1360 ) {

            if ( align === 'left') {
              gsapAnimation = {
                left: -jQuery( window ).width(),
                onComplete: gsapCloseComplete
              };
            }
  
            if ( align !== 'left' ) {
              gsapAnimation = {
                right: -jQuery( window ).width(),
                onComplete: gsapCloseComplete
              };
            }

          }
          else {
            if ( align === 'left') {
              gsapAnimation = {
                left: -ldSidenav.width(),
                onComplete: gsapCloseComplete
              };
            }
  
            if ( align !== 'left' ) {
              gsapAnimation = {
                right: -ldSidenav.width(),
                onComplete: gsapCloseComplete
              };
            }
          }

          var gsapCloseComplete = function() {
            events.callbacks.animation.close();
            events.callbacks.animation.both();
          };

          if ( ldSidenav.width() > 668 ) {
            TweenLite.to( ldSidenav, gsapDurationInSecondsLg, gsapAnimation );
          }
          if ( ldSidenav.width() <= 668 ) {
            TweenLite.to( ldSidenav, gsapDurationInSeconds, gsapAnimation );
          }

          // Apply the animation, the options and the callbacks 
          // TweenLite.to(ldSidenav, duration, {animation, easing, callbacks}); 
          // ldSidenav.animate( animation, duration, easing, callbacks ); 

          events.on.animation.close();
          events.on.animation.both();
        };

      // Create the sidebar style 
      ldStyle = {
        position: "fixed",
        top: parseInt( ldSidenavConfig.top ),
        bottom: 0,
        width: setSidenavWidth( width ),
        zIndex: ldSidenavConfig.zIndex
      };

      // Set initial position 
      ldStyle[ align ] = ( "closed" === init ) ? -setSidenavWidth( width ) : 0;

      // freeze page if sidebar is opened 
      if ( scrollCfg && "opened" === init ) {
        freezePage();
      }

      // Apply style to the sidebar 
      ldSidenav.css( ldStyle )
        .attr( attr, init ); // apply init 

      // Create the private mask style 
      pvtMaskStyle = {
        position: "fixed",
        top: parseInt( ldSidenavConfig.top ),
        right: 0,
        bottom: 0,
        left: 0,
        zIndex: 2,
        display: "none"
      };

      // Hide or show the mask according to the chosen init option 
      pvtMaskStyle.display = ( "opened" === init ) ?
        "block" :
        "none";

      // Merge the Mask private and custom style but keep private style unchangeable 
      maskStyle = jQuery.extend( true, pvtMaskStyle, ldSidenavConfig.mask.css );

      // Create Mask if required 
      // Mask is appended to body 
      if ( ldSidenavConfig.mask.display ) {
        createMask();
      }

      // Apply animations 
      ldSidenavTrigger.click( function() {
        switch ( sidebarStatus() ) {
          case "opened":
            animateClose();
            break;
          case "closed":
            animateOpen();
            break;
        }
      } );

      // ldSidenavMask.click( animateClose );
      ldSidenavMask.on( "click", ldSidenavClose, animateClose );
      // ldSidenav.on( "click", ldSidenav, animateClose );

      // Make the sidebar responsive 
      function throttledWidthCheck() {
        var width = jQuery( window )
          .width();
        ldSidenav.css( "width", setSidenavWidth( width ) );
        if ( "closed" === sidebarStatus() ) {
          if ( ldSidenav[ 0 ][ 'id' ] === 'sidebar-right-nav' && jQuery( window )
            .width() <= 667 ||
            ldSidenav[ 0 ][ 'id' ] === 'sidebar-right-enquire' && jQuery( window )
            .width() <= 1360 ) {
            ldSidenav.css( align, -jQuery( window )
              .width() );
          }
          else {
            ldSidenav.css( align, -ldSidenav.width() );
          }
        }
      }
      window.addEventListener( 'resize', _.throttle( function() {
        throttledWidthCheck()
      }, 200 ) );
    } );

}

function initSidebars() {

  if ( jQuery('#sidebar-right-enquire').length > 0 ) {
    var ldSidenavRightEnquire = jQuery( "#sidebar-right-enquire" ).ldSidenav({
      top: 0, 
      gap: 0, 
      zIndex: 1401, 
      align: "right",
      attr: "nav-open",
      selectors: {
        trigger: "#sidebar-right-enquire-button"
      },
      sidebar: {
        width: 1088
      },
      events: {
        callbacks: {
          animation: {
            freezePage: true
          }
        }
      }
    });
  }

  if ( jQuery('#sidebar-left-nav').length > 0 ) {
    var ldSidenavLeft = jQuery( "#sidebar-left-nav" ).ldSidenav({
      top: 0, 
      gap: 0, 
      zIndex: 1502, 
      align: "left",
      attr: "nav-open",
      selectors: {
        trigger: "#sidebar-left-nav-button"
      },
      sidebar: {
        width: 992
      },
      events: {
        callbacks: {
          animation: {
            freezePage: true
          }
        }
      }
    });
  }

}

function leftPadOffset() {
  var navbarBrand = jQuery('#myStirlingNavbarBrand');
  var windowWidth = jQuery( window ).width();
  var containerWidth = jQuery( '#wrapperContainerReference > .container' ).width();
  var offsetAmount = ( windowWidth - containerWidth ) / 2;
  var leftOffset = offsetAmount - 92; 
  if ( windowWidth > 1367 ) {
    navbarBrand.animate( {left:leftOffset}, {duration:100, easing:"easeOutStrong"});
  }
  if ( windowWidth <= 1367 ) {
    navbarBrand.animate( {left:2}, {duration:100, easing:"easeOutStrong"});
  }
  console.log('leftOffset is currently: '+leftOffset);
}

function leftColumnOffset() {
  if (jQuery('.left-column-offset').length > 0 ) {
    var leftColumn = jQuery('.left-column-offset');
    var leftColumnwindowWidth = jQuery( window ).width();
    var leftColumncontainerWidth = jQuery( '#wrapperContainerReference > .container' ).width();
    var leftColumnleftOffset = ( leftColumnwindowWidth - leftColumncontainerWidth ) / 2;
    if ( leftColumnwindowWidth > 619 ) {
      leftColumn.animate( {paddingLeft:leftColumnleftOffset}, {duration:100, easing:"easeOutStrong"});
    }
    if ( leftColumnwindowWidth <= 619 ) {
      leftColumn.animate( {paddingLeft:0}, {duration:100, easing:"easeOutStrong"});
    }
    console.log('leftOffset is currently: '+leftColumnleftOffset);
  }
}

jQuery( window ).resize( _.throttle( function() {
  leftPadOffset();
  leftColumnOffset();
}, 125 ) );
