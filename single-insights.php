<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$siteURL = get_bloginfo('url');
$themeURL  = get_stylesheet_directory_uri();
$siteTitle = get_bloginfo('Title');
$themePath = get_stylesheet_directory();
if ( get_field('banner_image') ) {
  $banner_image = get_field('banner_image');
  if ( is_array( $banner_image ) && array_key_exists( 'url', $banner_image ) ) {
    $banner_image = $banner_image['url'];
  }
} else {
  $banner_image = get_stylesheet_directory_uri() . '/img/single-insight.jpg';
}
$gallery_images = get_field( 'gallery' ); 
?> 
<div class="wrapper" id="single-wrapper">
  <div class="container-fluid" id="content" tabindex="-1">
    <div class="row">
      <div class="col content-area" id="primary">  
        <div class="entry-content">
            <div class="container-fluid bg-light">
              <div class="subpage-banner row">
                <img src="<?php echo $banner_image; ?>" class="img-fit ">
              </div>
              <div class="container container-inner container-insights-content gutters pt-5 pt-lg-7">
                <div class="row mb-5">
                  <div class="col-48 gutters mx-auto">
                    <h2 class="entry-title justify-self-center align-self-center mb-4"><?php the_title();?></h2>
                    <span class="entry-date">Posted <?php echo get_the_date('d . m . y'); ?></span>
                  </div>
                </div>
                <div class="row">
                  <div class="col-48 col-lg-35 gutters mb-5 mx-auto"> 
<?php
if ( have_posts() ) {
  while ( have_posts() ) {
    the_post(); 
    the_content();
  } 
}
?>
                   </div>
                  <div class="col-48 col-lg-35 mx-auto gutters"> 
<?php
if ( get_field( 'gallery' ) ) {
  foreach ( $gallery_images as $gallery_image ) {
?>
                    <img class="mb-4" src="<?php echo $gallery_image['url']; ?>" />
<?php 
  }
}
?>
                  </div>
                  <div class="col-48 col-lg-35 mb-5 mb-lg-2 mx-auto gutters">
                    <a class="btn btn-link btn-outline-dark btn-hover-primary p-4" href="/insights"> back to insights </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div><?php // need this extra closing tag ?>
</div>


<?php get_footer();
