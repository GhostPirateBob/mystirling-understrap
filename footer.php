<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}
$siteURL = get_bloginfo('url');
$themeURL  = get_stylesheet_directory_uri();
$siteTitle = get_bloginfo('Title');
$themePath = get_stylesheet_directory();
?>

<div class="wrapper container-fluid" id="wrapper-footer">

<?php
  global $post;
  $itemID = 987;
if ( is_singular() && is_object($post)) {
  $itemID = $post->ID;
} else {
  $itemID = 987;
}

if ( is_archive() || is_page( 'apartment' ) || is_singular('insights') || is_page('apartments')  ) {
  $archive_type = get_queried_object()->name;
  if ( $archive_type === 'developments' ) {
    $archivePagePath = get_page_by_path( 'developments-page');
    $itemID = $archivePagePath->ID;
    if ( isset( $itemID ) || !$itemID > 0 ) {
      $itemID = 700;
    }
  }
  if ( $archive_type === 'apartment' || is_singular( 'apartment' ) ) {
    $archivePagePath = get_page_by_path( 'apartments-page');
    $itemID = $archivePagePath->ID;
    if ( isset( $itemID ) || !$itemID > 0 ) {
      $itemID = 987;
    }
  }
  if ( $archive_type === 'insights' || is_singular('insights') ) {
    $archivePagePath = get_page_by_path( 'insights-page');
    $itemID = $archivePagePath->ID;
    if ( isset( $itemID ) || !$itemID > 0 ) {
      $itemID = 1264;
    }
  }
  if (is_page('apartments') || is_page('apartment')) {
    $itemID = 987;
  }
}
if ( $itemID && is_numeric( $itemID ) && 
      get_field( 'page_footer_background_image', $itemID ) && 
      get_field( 'page_footer_contact_form', $itemID ) ) {
  $background_image = get_field( 'page_footer_background_image', $itemID );
  $contact_form_id  = get_field( 'page_footer_contact_form', $itemID ); 
  $contact_form_id  = $contact_form_id['id'];
?>
<div class="container-fluid footer-form-wrapper bg-dark">
  <img class="img-fluid img-underlay-footer" src="<?php echo $background_image['url']; ?>" />
  <div class="row row-footer-contact-area">
    <div class="container">
      <div class="row">
        <div class="col-48 col-xl-30 footer-contact-col gutters mx-auto">
          <div class="footer-contact-form dp-0 bg-white px-3 px-lg-6 py-3 py-lg-6"  data-aos="fade-up">
            <?php if ( is_singular('promotions') ) { ?>
              <h2 class="text-dark mb-5 gutters"><small class="text-bold fw-700"> Enter Now </small></h2>
              <?php gravity_form( $contact_form_id, false, false, false, '', true, $contact_form_id * 100 ); ?>
            <?php } ?>
            <?php if ( is_page('apartment') ) { ?>
            <h2 class="text-dark mb-3 mb-sm-5 pt-4 pt-sm-0 gutters"><small class="text-bold fw-700 ls-1"> Sales <br /> Enquiries </small></h2>
            <h5 class="mb-5 fw-500 gutters">
              <a href="tel:<?php the_field('phone_dial', 'options'); ?>"><span class="ls-0"> <?php the_field('phone', 'options'); ?> </span></a>
              &nbsp;&nbsp;
              <a href="mailto:sales@stirlingcapital.com.au">sales@stirlingcapital.com.au</a>
            </h5>
            <?php gravity_form( 3, false, false, false, '', true, 3 * 100 ); ?>
            <?php } ?>
            <?php if ( !is_page('apartment') && !is_singular('promotions') ) { ?>
            <h2 class="text-dark mb-3 mb-sm-5 pt-4 pt-sm-0 gutters"><small class="text-bold fw-700 ls-1"> Sales <br /> Enquiries </small></h2>
            <h5 class="mb-5 fw-500 gutters">
              <a href="tel:<?php the_field('phone_dial', 'options'); ?>"><span class="ls-0"> <?php the_field('phone', 'options'); ?> </span></a>
              &nbsp;&nbsp;
              <a href="mailto:sales@stirlingcapital.com.au">sales@stirlingcapital.com.au</a>
            </h5>
            <?php gravity_form( $contact_form_id, false, false, false, '', true, $contact_form_id * 100 ); ?>
            <?php } ?>
          </div>
          <div class="footer-contact-border dp-00"></div>
        </div>
      </div>
    </div> 
  </div>
</div>
<?php } ?>
  <div class="container-fluid wrapper-subscribe">
    <div class="row row-footer-nav-area pt-5 pb-0 pt-lg-0">
      <div class="container container-inner">
        <div class="row">
          <div class="col-48 col-xl-14 gutters col-footer-nav col-footer-nav-left px-md-5 py-4 pt-sm-0 pt-lg-4 order-2 order-lg-1">
            <div class="footer-img-address-wrapper w-100">  
              <div class="footer-img-wrapper img-logo-bot d-block mt-4 mb-4" data-aos="fade-right">
              <a type="link" href="//www.stirlingcapital.com.au/"
                    target="_blank">
              <?php echo file_get_contents( $themePath . '/img/logo-stirling-capital.svg'); ?>
                <p class="text-white mb-2 footer-wrapper-text d-block">
                  Visit the Stirling<br> corporate website<br>
                </p>
                <?php echo file_get_contents($themePath . '/img/arrow-right.svg'); ?> 
              </a>
              </div>
            </div>
          </div>

          <div class="col-48 col-xl-34 gutters col-footer-nav col-footer-nav-right pt-2 pt-md-5 px-2 px-md-5  text-white order-1 order-lg-2">
            <div class="row row-footer-content" data-aos="fade-left">

              <div class="col-48 gutters mb-2 mb-lg-5">
                <?php echo file_get_contents( $themePath . '/img/logo-stirling.svg'); ?>
              </div>

              <div class="col-48 gutters" data-aos="fade-left">
                <h4 class="footer-subscribe text-primary mb-2 mt-2 mt-lg-5 mb-lg-5"> Subscribe to myStirling Life for the latest insights, promotions and exclusive giveaways. </h4>
              </div>

                <div class="col-48 internal-gutters mb-5" data-aos="fade-left">
                  <?php gravity_form( 2, false, false, false, '', true, ( 2 * 100 ) ); ?>
                </div>
              </div>

              <div class="col-48 internal-gutters mb-0 pb-0 mb-sm-5 pb-sm-5" data-aos="fade-left">
                <div class="row row-footer-multisite-nav">
                  <div class="col-48 col-lg-16 col-xl-15 gutters">
                    <p class="footer-multisite-link mb-3 text-primary text-btn"> myStirling </p>
  <?php wp_nav_menu(
    array(
      'menu'            => 'footer_nav',
      'theme_location'  => 'footer_nav',
      'menu_id'         => 'footer-menu',
      'fallback_cb'     => false,
      'container_class' => '',
      'container'       => '',
      'container_id'    => '',
      'menu_class'      => 'list-unstyled footer-nav-project navbar-light',
      'depth'           => 1,
      'walker'          => new NavWalker(),
      'echo'            => true,
      'before'          => '',
      'after'           => '',
      'link_before'     => '',
      'link_after'      => ''
    )
  ); ?>
                  </div>
                  <div class="col-48 col-lg-16 col-xl-15 gutters">
                    <p class="footer-multisite-link mb-3 text-primary text-btn"> Developments </p>
  <?php wp_nav_menu(
    array(
      'menu'            => 'footer_mystirling_nav',
      'theme_location'  => 'footer_mystirling_nav',
      'menu_id'         => 'footer-menu',
      'fallback_cb'     => false,
      'container_class' => '',
      'container'       => '',
      'container_id'    => '',
      'menu_class'      => 'list-unstyled footer-nav-project navbar-light',
      'depth'           => 1,
      'walker'          => new NavWalker(),
      'echo'            => true,
      'before'          => '',
      'after'           => '',
      'link_before'     => '',
      'link_after'      => ''
    )
  ); ?>
                  </div>
                  <?php if ( get_field('facebook_url', 'option') || get_field('instagram_url', 'option') ) { ?>
                  <div class="col-48 col-lg-16 col-xl-17 gutters">
                    <p class="footer-multisite-link mb-3 text-primary text-btn"> Social Media </p>
                    <?php if ( get_field('instagram_url' , 'option') ) { ?>
                    <a href="<?php echo strip_tags(get_field('instagram_url', 'option')); ?>" class="footer-multisite-social-link mr-2" target="_blank" rel="nofollow">
                      <i class="fa fa-instagram text-white"></i>
                    </a>
                    <?php } ?>
                    <?php if ( get_field('facebook_url', 'option') ) { ?>
                    <a href="<?php echo strip_tags(get_field('facebook_url', 'option')); ?>" class="footer-multisite-social-link" target="_blank" rel="nofollow">
                      <i class="fa fa-facebook text-white"></i>
                    </a>
                    <?php } ?>
                  </div>
                  <?php } ?>
                </div>
              </div>
            </div>
            <div class="col-48 col-xl-34 px-0 px-md-5 col-copyright gutters py-3 mb-4 d-flex flex-md-row flex-column justify-content-between order-lg-3 order-3 ml-auto">
              <p class="text-muted gutters">
                Copyright &copy; <?php echo date('Y'); ?>
              <?php if ( get_page_by_path('privacy-policy') ) { 
                $privacy_policy = get_page_by_path('privacy-policy'); ?>
                <span class="vertical-sep"></span>
                <a class="text-muted gutters" href="<?php echo get_the_permalink($privacy_policy->ID); ?>"> Privacy Policy </a>
              </p> 
              <?php } ?>
              <p class="text-muted gutters"> Creative by Design Collision </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

<?php wp_footer(); ?>

<?php if (is_page('apartments') || is_front_page()) { ?>
  <script src="<?php echo get_stylesheet_directory_uri() . '/app/dist/app-chunk.js'; ?>"></script>
  <script src="<?php echo get_stylesheet_directory_uri() . '/app/dist/app.js'; ?>"></script>
<?php } ?>

  <div class="modal fade" id="modalCenter" tabindex="-1" role="dialog" aria-labelledby="modalCenterTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h2 class="text-dark mb-4"><small class="text-bold fw-700 ls-0"> Request an<br /> Appointment </small></h2>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <?php gravity_form( 12, false, false, false, '', true, 12 * 100 ); ?>
        </div>
      </div>
      <div class="footer-contact-border dp-2"></div>
    </div>
  </div>

  <script>
  jQuery('#modalCenter').on('show.bs.modal', function (event) {
    var button = jQuery(event.relatedTarget)
    var returnMessage = button.data('project-title') + ': ' + button.data('project-id').toString();
    jQuery('#input_12_11').val(returnMessage);
  })
  </script>

</body>

</html>
