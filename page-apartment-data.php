<?php
/**
 * Template Name: Apartment Data Page
 * @package mystirling
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly.
}

// get_header();
$siteURL         = get_bloginfo('url');
$themeURL        = get_stylesheet_directory_uri();
$siteTitle       = get_bloginfo('Title');
$themePath       = get_stylesheet_directory();

$token = '3b834b2889800d9e';

if ( isset($_GET) && array_key_exists('token', $_GET) && $_GET['token'] === $token ) {
  header( "Access-Control-Allow-Origin: *" );
  header( "Access-Control-Allow-Methods: GET" );
  header( "Content-Type: application/json" );
  require_once( get_stylesheet_directory() . '/requires/require-apartment-array.php' );
  if ( file_exists( get_stylesheet_directory() . '/data/wordpress.json' ) === false ) {
    file_put_contents( get_stylesheet_directory() . '/data/wordpress.json',  json_encode($apartmentArray) );
  }
  if (array_key_exists('format', $_REQUEST) && $_REQUEST['format'] === 'array') {
    echo json_encode($apartmentArray);
  }
  if (!array_key_exists('format', $_REQUEST)) {
    echo json_encode($apartmentObject);
  }
  exit;
}

header("HTTP/1.1 301 Moved Permanently"); 
header("Location: " . $siteURL . "/apartments"); 
exit;
