<div id="homepageFeaturedApartments" class="row bg-light pt-6 pb-7">
  <div class="col-48">
    <div class="container container-inner">
      <div class="row">
        <div class="col-48">
          <?php if (is_page('apartment')) { ?>
            <h2 class="mb-5 pl-4 pl-lg-0"> Other Apartments you <br class="d-block d-sm-none">might be Interested in</h2>
          <?php } else { ?>
          <h2 class="mb-5 pl-4 pl-lg-0"> Featured Apartments <br class="d-block d-sm-none">for Sale</h2>
          <?php } ?>
        </div>
      </div>
    </div>
    <div class="container">
      <div id="featuredApartments" class="row">

<?php

require_once( get_stylesheet_directory() . '/requires/require-apartment-array.php' );

$i = 0;
foreach ( $apartmentObject as $apartmentID => $post ) {  
  if ( $i <= 7 ) {
    require( get_stylesheet_directory() . '/loop-templates/loop-featured-apartments.php' );
  }
  $i++;
}

?>

      </div>
      <div id="featuredApartmentsNavigation" class="slick-navigation mb-6"></div>
    </div>
  </div>
</div>
