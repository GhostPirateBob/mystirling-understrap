<?php 
  $treehouse_link = get_field('treehouse_link'); 
  $cirque_link = get_field('cirque_link'); 
  $barque_link = get_field('barque _link'); 
  $twentysix_link = get_field('26_link'); 
  $verdant_link = get_field('verdant_link'); 
?>



<div class="container mb-5">
  <div class="row">
    <div class="col-32 col-xl-auto gutters mr-auto z-10">
      <h2 class="page-title text-secondary adjustDown bg-primary mb-0" data-aos="fade-up"> My Perfect Place <br class="d-none d-sm-block"> is Here</h2>
    </div>
  </div>
  <div class="row internal-gutters mb-lg-3 mb-2 mt-neg-8">
    <div class="col-20 pr-lg-2 pr-1 overlay-wrapper" data-aos="fade-right">
    <?php if( $treehouse_link ) { ?>
    <a target="_blank" href="<?php echo esc_url($treehouse_link); ?>">
      <img class="img-fit" src="<?php echo $treehouse_image['url']; ?>" />
    </a>
    <?php } else { ?>
    <img class="img-fit" src="<?php echo $treehouse_image['url']; ?>" />
    <?php } ?>  
      <div class="overlay d-none d-lg-block">
        <span class="overlay-text"> <?php echo $treehouse_image['caption']; ?> </span>
      </div>
      <img class="overlay-img d-none d-lg-block"
        src="https://loadeddev.online/wp-content/uploads/treehouse-logo-white-final-300x129.png" />
    </div>
    
    <div class="col-28 pl-lg-2 pl-1 overlay-wrapper" data-aos="fade-left">
    <?php if( $cirque_link ) { ?>
    <a target="_blank" href="<?php echo esc_url($cirque_link); ?>">
      <img class="img-fit" src="<?php echo $cirque_image['url']; ?>" />
    </a>
    <?php } else { ?>
    <img class="img-fit" src="<?php echo $cirque_image['url']; ?>" />
    <?php } ?>  
      <div class="overlay d-none d-lg-block">
        <span class="overlay-text"> <?php echo $cirque_image['caption']; ?> </span>
      </div>
      <img class="overlay-img d-none d-lg-block"
        src="https://loadeddev.online/wp-content/uploads/cirque-logo-white-1-300x129.png" />
    
    </div>
  </div>
  <div class="row internal-gutters mb-lg-3 mb-2">
    <div class="col-43 ml-auto">
      <div class="row">
        <div class="col-24 pr-lg-2 pr-1 overlay-wrapper" data-aos="fade-right">
        <?php if( $barque_link ) { ?>
        <a target="_blank" href="<?php echo esc_url($barque_link); ?>">
          <img class="img-fit" src="<?php echo $barque_image['url']; ?>" />
        </a>
        <?php } else { ?>
        <img class="img-fit" src="<?php echo $barque_image['url']; ?>" />
        <?php } ?>  
          <div class="overlay d-none d-lg-block">
            <span class="overlay-text"> <?php echo $barque_image['caption']; ?> </span>
          </div>
          <img class="overlay-img d-none d-lg-block"
            src="https://loadeddev.online/wp-content/uploads/barque-dev-logo-white-300x129.png" />
        </div>
        <div class="col-24 pl-lg-2 pl-1 overlay-wrapper" data-aos="fade-left">
      <?php if( $twentysix_link ) { ?>
        <a target="_blank" href="<?php echo esc_url($twentysix_link); ?>">
          <img class="img-fit" src="<?php echo $twentysix_image['url']; ?>" />
        </a>
      <?php } else { ?>
        <img class="img-fit" src="<?php echo $twentysix_image['url']; ?>" />
      <?php } ?>  
          <div class="overlay d-none d-lg-block">
            <span class="overlay-text"> <?php echo $twentysix_image['caption']; ?> </span>
          </div>
          <img class="overlay-img d-none d-lg-block"
            src="https://loadeddev.online/wp-content/uploads/26-logo-white-1-300x129.png" />
        </div>
      </div>
    </div>
  </div>
  <div class="row internal-gutters mb-lg-3 mb-2">
    <div class="col-40 mr-auto  overlay-wrapper" data-aos="fade-up">
    <?php if( $verdant_link ) { ?>
    <a target="_blank" href="<?php echo esc_url($verdant_link); ?>">
      <img class="img-fit" src="<?php echo $verdant_image['url']; ?>" />
    </a>
    <?php } else { ?>
    <img class="img-fit" src="<?php echo $verdant_image['url']; ?>" />
    <?php } ?>  
      <div class="overlay d-none d-lg-block">
        <span class="overlay-text"> <?php echo $verdant_image['caption']; ?> <span>
      </div>
      <img class="overlay-img d-none d-lg-block"
        src="https://loadeddev.online/wp-content/uploads/verdant-white-logo-300x129.png" />
    </div>
  </div>
</div>
