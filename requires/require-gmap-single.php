<script type="text/javascript">
function initMap() {
  var mapOptions = {
  center: new google.maps.LatLng(-31.97,115.85),
  zoom: 11, 
  mapTypeControl: false,
  streetViewControl: false,
  disableDefaultUI: true,
  styles: [ { "featureType": "all", "elementType": "all", "stylers": [ { "saturation": "32" }, { "lightness": "-3" }, { "visibility": "on" }, { "weight": "1.18" } ] }, { "featureType": "administrative", "elementType": "labels", "stylers": [ { "visibility": "on" } ] }, { "featureType": "landscape", "elementType": "labels", "stylers": [ { "visibility": "off" } ] }, { "featureType": "landscape.man_made", "elementType": "all", "stylers": [ { "saturation": "-70" }, { "lightness": "14" } ] }, { "featureType": "poi", "elementType": "labels", "stylers": [ { "visibility": "off" } ] }, { "featureType": "road", "elementType": "labels", "stylers": [ { "visibility": "off" } ] }, { "featureType": "transit", "elementType": "labels", "stylers": [ { "visibility": "off" } ] }, { "featureType": "water", "elementType": "all", "stylers": [ { "saturation": "100" }, { "lightness": "-14" } ] }, { "featureType": "water", "elementType": "labels", "stylers": [ { "visibility": "off" }, { "lightness": "12" } ] } ]
};
  map = new google.maps.Map(document.getElementById('map'), mapOptions);
  var features = [ <?php 
  $i = 0;
  $building_id = $apartmentObject[$apartmentAltID]['rex_building_id'];
  $website_link = $apartmentObject[$apartmentAltID]['development_website_link'];
  $website_link_url = $website_link['url'];
  if ( $apartmentObject[$apartmentAltID]['development_google_map']['marker_image'] ) {
    $icon_url = $apartmentObject[$apartmentAltID]['development_google_map']['marker_image'];
    $icon_url = $icon_url['url'];
  } else {
    $icon_url = "https://s3-ap-southeast-2.amazonaws.com/loaded-public/loaded-static/img/mystirling-map-marker.png";
  }

    $google_map_location = $apartmentObject[$apartmentAltID]['development_google_map']['google_map_location'];
    $google_map_address = $apartmentObject[$apartmentAltID]['development_google_map']['google_map_location']['address'];
    $google_map_lat = $apartmentObject[$apartmentAltID]['development_google_map']['google_map_location']['lat'];
    $google_map_lng = $apartmentObject[$apartmentAltID]['development_google_map']['google_map_location']['lng'];
    $google_map_geocode = $google_map_lat . ',' . $google_map_lng;

    echo '{ position: new google.maps.LatLng(' . $google_map_geocode . '), ';
    echo ' icon: "' . $icon_url . '", ';
    echo 'content: "';
      echo '<div class=\"card bg-dark b-0 gmap-info-body\" style=\"max-width: 226px;\">';
        echo '<div class=\"card-body\">';
        echo '<p class=\"card-title gmap-info-title text-uppercase fw-400 mb-0\">' . $apartmentObject[$apartmentAltID]['address_building_name'] . '</p>';
          echo '<p class=\"card-title gmap-info-title text-uppercase text-primary fw-400 mb-3\">' . 'Apt ' . $apartmentObject[$apartmentAltID]['address_unit_number'] . '</p>';
          echo '<p class=\"card-subtitle mb-2\">' . $apartmentObject[$apartmentAltID]['address_street_number'] . ' ' . $apartmentObject[$apartmentAltID]['address_street_name'] .','.'</p>';
          echo '<p class=\"card-subtitle mb-2\">' . $apartmentObject[$apartmentAltID]['address_suburb_or_town'] . '</p>';
      echo '</div>';
      echo '</div>';
    echo '" },';
  
  $i++;
?> ];

  //create markers
  features.forEach(function(feature) {
    var marker = new google.maps.Marker({
      position: feature.position,
      type: feature.type,
      map: map,
      title: feature.title,
      icon: feature.icon,
      info: feature.info
    });
    var infowindow = new google.maps.InfoWindow({
        content: feature.content
    });
    marker.addListener('click', function() {
        jQuery('.gm-style-iw').parent().hide();
        infowindow.open(map, marker);
    });
  });

  if ( jQuery('#expandMap').length > 0 ) {
    jQuery('#expandMap').appendTo('#map');
  }

}
</script>
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyB0Pciba8hNGj3oIlh3UqWqUXWz6f_EK8s&callback=initMap" type="text/javascript"></script>

