<div class="promotionsContainer container mb-3 mb-lg-5 pt-6">
  <div class="row">
    <div class="container container-inner gutters">
      <div class="row mb-5" data-aos="fade-up">
        <h2> <?php echo $promotional_block_heading; ?> </h2>
      </div>
    </div>
    <div class="row card-deck w-100" data-aos="fade-up">
      <div class="card-deck">
<?php
$query = new WP_Query(array(
  'posts_per_page' => 2,
  'post_type'      => 'promotions',
  'order'          => 'DESC',
));
$count = 0;

$output_array = array();

while ($query->have_posts()) {
  $query->the_post();
  $promo_text          = get_field('promo_text');
  $promo_teaser_image  = get_field('promo_teaser_image');
  $development_logo    = get_field('development_logo');

  if ($development_logo) {
  $development_logo_id = $development_logo->ID;
  $logo_black          = get_field('logo_black', $development_logo_id);
  $logo_white          = get_field('logo_white', $development_logo_id);
  

  // if ($background_color === 'dark') {
  //   $output_array['dark_items'][] = $development_logo_id;
  // }
  // if ($background_color === 'info') {
  //   $output_array['info_items'][] = $development_logo_id;
  // }
}

  $promo_button        = get_field('promo_button');
  $background_color    = get_field('promo_teaser_background_color');
  $promotion_description = get_field('promotion_description');
  $text_color = get_field('promo_teaser_text_color');
  $count_posts = wp_count_posts( 'promotions' )->publish;
?>


 <?php if ( $count_posts !== "1" ) { ?>
        <div class="card b-0 bg-<?php echo $background_color ?>">
          <img src="<?php echo $promo_teaser_image['url']; ?>" class="card-img-top w-100" />
          <div class="card-body">
            <p class="card-text text-<?php echo $text_color; ?> "> <?php echo $promo_text; ?> </p>
          </div>
          <div class="card-footer">
            <div class="row">
              <div class="col-48 d-flex py-3 py-lg-4  flex-row justify-content-between align-items-center">
              <?php if ($promo_button) { ?>
                <a class="text-<?php echo $text_color; ?> btn btn-link btn-outline-<?php echo $text_color; ?>"
                  href="<?php echo $promo_button['url']; ?>" 
                  <?php echo $promo_button['target']; ?> >
                  <?php echo $promo_button['text']; ?> 
                </a> 
              <?php } ?>
              <?php if (!$development_logo) {
          $logo = get_field('logo', 'option');?>
          <?php loadedImage($logo, ['d-inline-block', 'align-top', 'img-fluid']); ?>
          <?php } else { ?>
              <?php if ($text_color === 'dark') { ?>
                <img  class="logo" src="<?php echo $logo_black['url']; ?>" /> 
              <?php } else { ?> 
                <img class="logo" src="<?php echo $logo_white['url']; ?>" /> 
              <?php } ?>
              <?php } ?> 
              </div>
            </div>
          </div>

        </div>
              <?php } ?>
<?php $count++; } wp_reset_query(); ?> 
      </div>


      <?php if ( $count_posts !== "1" ) { ?>
      <div id="promotionsNavigation" class="slick-navigation mb-6"></div>
      <?php } ?>
    </div>

    <?php if ( $count_posts == "1" ) { ?>
    
        <div class="row w-100 gutters">
      <div class="col-48 col-lg-32">
        <img src="<?php echo $promo_teaser_image['url']; ?>" class="img-fit" />
      </div>
      <div class="col-48 col-lg-16 bg-secondary p-3 py-4 p-lg-5 d-flex flex-column justify-content-between">
        <?php if (!$development_logo) {
          $themePath = get_stylesheet_directory(); ?>
          <div class="mb-4"> <?php echo file_get_contents( $themePath . '/img/logo-stirling.svg'); ?></div>
          <?php } else { ?>
        <img style="max-width: 130px;" class="logo mb-4" src="<?php echo $logo_white['url']; ?>" />
        <?php } ?>
        <p style="font-size:1.125rem;" class="text-white mb-4"><?php echo $promo_text; ?></p>
      
        
        <?php if ($promo_button) { ?>
                <a class="text-white btn btn-link btn-outline-white p-3 align-self-start"
                  href="<?php echo $promo_button['url']; ?>" 
                  <?php echo $promo_button['target']; ?> >
                  <?php echo $promo_button['text']; ?> 
                </a> 
              <?php } ?>
      </div>
    </div>
        <?php } ?>
  </div>
</div>
