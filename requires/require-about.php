<div class="about-wrapper container-fluid bg-secondary py-5 pt-lg-7">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-48 col-lg-23 mr-auto" data-aos="fade-down">
          <img class="img-fluid" src="<?php echo $overlap_image['url']; ?>" />
        </div>
        <div class="row adjustmentRow w-100">
          <div class="container container-inner">
            <div class="row d-flex align-items-center">
              <div class="col-48 col-lg-25 gutters pr-lg-4 mb-5 mb-lg-0 " data-aos="fade-up">
                <div class="bg-primary py-4 py-lg-0  p-lg-5">
                  <h2 class="text-secondary font-weight-bold mw-lg-27 ml-lg-4 gutters mb-4">
                    <?php echo strip_tags(get_field( 'about_heading' )); ?>
                  </h2>
                  <h5 class="fw-400 ls-0 mw-lg-27 ml-lg-4 gutters mb-4"> <?php the_field( 'about_content' ); ?>
                  </h5>
                  <div class="mw-lg-27 ml-lg-4 gutters mb-4">
                    <a class="btn btn-link btn-arrow-right btn-arrow-right-dark btn-arrow-right-hover-white mt-auto"
                      type="link" href="https://www.stirlingcapital.com.au/" target="_blank">
                      <span class="btn-arrow-text">
                        Stirling Corporate Website
                        <?php echo file_get_contents($themePath . '/img/arrow-right.svg'); ?> 
                      </span>
                    </a>
                  </div>
                </div>
              </div>
              <div class="col-48 col-lg-23 gutters pl-lg-5" data-aos="fade-up">
                <div class="row">
                  <img class="img-fit mb-5" src="<?php echo $team_image['url']; ?>" />
                </div>
                <div class="row">
                  <h3 class="text-primary callout_heading mb-5"> <?php echo strip_tags(get_field( 'callout_heading' )); ?> </h3>
                </div>
                <div class="row">
                  <p class="text-white team_content"> <?php echo strip_tags(get_field( 'team_content' )); ?> </p>
                  <a class="btn btn-link btn-arrow-right btn-arrow-right-white btn-arrow-right-hover-primary mt-auto" type="link"
                    href="<?php the_field( 'website_link' ); ?>">
                    <span class="btn-arrow-text">
                      Read More
                      <?php echo file_get_contents($themePath . '/img/arrow-right.svg'); ?> 
                    </span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
