<div class="container pb-3 pb-lg-7">
  <div class="row mb-2 mb-lg-5">
    <div class="container container-inner">
      <div class="row mb-2 mb-lg-5" data-aos="fade-up">
        <div class="col-48 gutters">
          <h2>#<span class="font-weight-normal">my</span>Stirling</h2>
        </div>
      </div>
      <div class="row instaConnect d-flex align-items-center" data-aos="fade-up">
        <div class="col-48 gutters text-uppercase font-weight-bold "> connect with us 
          <a href="https://www.instagram.com/stirlingcapital/" 
            class="footer-multisite-social-link ml-lg-4 mr-2"
            target="_blank" rel="nofollow">
            <i class="fa fa-instagram text-dark ml-2"></i>
          </a>
          <a href="https://www.facebook.com/mystirling/" 
            class="footer-multisite-social-link" 
            target="_blank" rel="nofollow">
            <i class="fa fa-facebook text-dark"></i>
          </a>
        </div>
      </div>
    </div>
  </div>
  <div class="row instagram-images" data-aos="fade-up">
    <div class="col-48">
      <div id="insta-feed" class="row row-insta-feed w-100"></div>
    </div>
  </div>
</div>
