<?php 

$siteURL         = get_bloginfo('url');
$themeURL        = get_stylesheet_directory_uri();
$siteTitle       = get_bloginfo('Title');
$themePath       = get_stylesheet_directory();
$perthLat        = -31.9592096;
$perthLng        = 115.8583212;
?>
<script type="text/javascript">
function initMap() {
  var mapOptions = {
  center: new google.maps.LatLng(<?php echo $perthLat; ?>,<?php echo $perthLng; ?>),
  zoom: 11, 
  mapTypeControl: false,
  streetViewControl: false,
  disableDefaultUI: true,
  styles: [ { "featureType": "all", "elementType": "all", "stylers": [ { "saturation": "32" }, { "lightness": "-3" }, { "visibility": "on" }, { "weight": "1.18" } ] }, { "featureType": "administrative", "elementType": "labels", "stylers": [ { "visibility": "on" } ] }, { "featureType": "landscape", "elementType": "labels", "stylers": [ { "visibility": "off" } ] }, { "featureType": "landscape.man_made", "elementType": "all", "stylers": [ { "saturation": "-70" }, { "lightness": "14" } ] }, { "featureType": "poi", "elementType": "labels", "stylers": [ { "visibility": "off" } ] }, { "featureType": "road", "elementType": "labels", "stylers": [ { "visibility": "off" } ] }, { "featureType": "transit", "elementType": "labels", "stylers": [ { "visibility": "off" } ] }, { "featureType": "water", "elementType": "all", "stylers": [ { "saturation": "100" }, { "lightness": "-14" } ] }, { "featureType": "water", "elementType": "labels", "stylers": [ { "visibility": "off" }, { "lightness": "12" } ] } ]
  };
  map = new google.maps.Map(document.getElementById('map'), mapOptions);
  var features = [ <?php 
$i = 0;
$query = new WP_Query(array(
  'post_type' => 'developments',
  'post_status' => 'publish',
  'posts_per_page' => -1
));
while ($query->have_posts()) {
  $query->the_post();
  $post_id = get_the_ID();
  $post_title = get_the_title();
  $website_link = get_field('website_link');
  $website_link_url = $website_link['url'];
  $apartment_filter_link = get_field('apartments_link');
  $apartment_filter_link_url = $apartment_filter_link['url'];
  if ( get_field( "google_map_marker_image" ) ) {
    $icon_url = get_field( "google_map_marker_image" );
    $icon_url = $icon_url['url'];
  } else {
    $icon_url = "https://s3-ap-southeast-2.amazonaws.com/loaded-public/loaded-static/img/mystirling-map-marker.png";
  }
  if ( get_field( "google_map_google_map_location" ) ) {
    $google_map_location = get_field( "google_map_google_map_location" );
    $google_map_address = $google_map_location['address'];
    $google_map_lat = $google_map_location['lat'];
    $google_map_lng = $google_map_location['lng'];
    $google_map_geocode = $google_map_lat . ',' . $google_map_lng;
    if ( get_field('address_one') ) {
      $address_one = strip_tags(get_field('address_one'));
    }
    if ( get_field('address_one') === false ) {
      $address_one = "";
    }
    if ( get_field('address_two') ) {
      $address_two = strip_tags(get_field('address_two'));
    }
    if ( get_field('address_two') === false ) {
      $address_two = "";
    }
    echo '{ position: new google.maps.LatLng(' . $google_map_geocode . '), ';
    echo 'type: '.$post_id.', title: "' . $post_title . '", icon: "' . $icon_url . '", ';
    echo 'content: "';
      echo '<div class=\"card bg-dark b-0 gmap-info-body\" style=\"max-width: 248px;\">';
        echo '<div class=\"card-body\">';
          echo '<p class=\"card-title gmap-info-title text-uppercase fw-400\">' . $post_title . '</p>';
          if ( get_field('address_two') && get_field('address_two') ) {
            echo '<p class=\"card-subtitle mb-2\">' . $address_one . '<br />' . $address_two . '</p>';
          }
          if ( get_field('address_two') && get_field('address_two') === false ) {
            echo '<p class=\"card-subtitle mb-2\">' . $address_one . '</p>';
          }
          if ( get_field('from_price') ) {
            $from_price = get_field('from_price');
            $from_price = number_format( $from_price );
            $from_price = '<sup class=\"from-price\">FR</sup>&dollar;' . $from_price;
            echo '<h5 class=\"gmap-info-from mb-3 fw-300 text-primary\">' . $from_price . '</h5>';
          }
          echo '<a href=\" ' . $website_link_url . ' \" class=\"btn btn-link btn-arrow-right btn-arrow-right-white btn-arrow-right-hover-primary mb-2\" > View Website <svg class=\"arrow-right img-fluid\" xmlns=\"http://www.w3.org/2000/svg\" version=\"1\" viewBox=\"0 0 36 8\" width=\"40px\"> <path fill=\"#fff\" class=\"arrow-right-path\" d=\"M0 3.5148983h35.2661133v.9783244H0v-.9783244zM31.6010742 8l-.6806641-.7165461L34.53125 4.0040603 30.9199219.7165461 31.6015625 0 36 4.0050159 31.6010742 8z\"/> </svg> </a>';
          echo '<a href=\"'. $apartment_filter_link_url .'\" class=\"btn btn-link btn-arrow-right btn-arrow-right-white btn-arrow-right-hover-primary\" > Find Apartments <svg class=\"arrow-right img-fluid\" xmlns=\"http://www.w3.org/2000/svg\" version=\"1\" viewBox=\"0 0 36 8\" width=\"40px\"> <path fill=\"#fff\" class=\"arrow-right-path\" d=\"M0 3.5148983h35.2661133v.9783244H0v-.9783244zM31.6010742 8l-.6806641-.7165461L34.53125 4.0040603 30.9199219.7165461 31.6015625 0 36 4.0050159 31.6010742 8z\"/> </svg> </a>';
        echo '</div>';
      echo '</div>';
    echo '" },';
  }
  $i++;
}
wp_reset_query();
?> ];

  //create markers
  features.forEach(function(feature) {
    var marker = new google.maps.Marker({
      position: feature.position,
      type: feature.type,
      map: map,
      title: feature.title,
      icon: feature.icon,
      info: feature.info
    });
    var infowindow = new google.maps.InfoWindow({
        content: feature.content
    });
    marker.addListener('click', function() {
        jQuery('.gm-style-iw').parent().hide();
        infowindow.open(map, marker);
    });
  });

  if ( jQuery('#expandMap').length > 0 ) {
    jQuery('#expandMap').appendTo('#map');
  }

}
</script>
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyB0Pciba8hNGj3oIlh3UqWqUXWz6f_EK8s&callback=initMap" type="text/javascript"></script>
