<div class="container-fluid bg-info mb-5">
  <div class="row">
    <div class="ourDevelopments container">
      <div class="row subpage-title mt-neg-7 mb-5" data-aos="fade-up">
        <div class="col-auto mr-auto gutters">
          <h2 class="page-title text-secondary bg-primary mb-0"> Our <br /> Developments </h2>
        </div>
      </div>
      <div id="developmentsSlider" class="row"> 
<?php $query = new WP_Query(array(
  'posts_per_page' => -1,
  'post_type'      => 'developments',
  'order'          => 'DESC'
));
$count = 0;
while ($query->have_posts()) {
  $query->the_post();
  $logo_black               = get_field('logo_black');
  $our_developments_image   = get_field('our_developments_image');
  $short_description        = get_field('short_description');
  $website_link             = get_field('website_link');
  $address_two              = get_field('address_two');
  $short_name               = get_field('short_name'); ?> 
        <div class="col-48 gutters" data-aos="fade-up">
          <img src="<?php echo $our_developments_image['url']; ?>" class="card-img-top">
          <div class="card">
            <div class="card-body">
              <img src="<?php echo $logo_black['url'] ?>" class="logo">
              <h5 class="card-title fw-400 mb-0"><strong> <?php echo $short_name;?>, </strong> <?php echo $address_two;?> </h5>
            </div>
            <div class="card-footer pt-0">
              <div class="row">
                <div class="col-48 d-flex py-2  flex-column flex-lg-row justify-content-start justify-content-lg-between align-items-start align-items-lg-center">
                  <p class="mw-lg-20 pr-4"><?php echo $short_description;?></p>
                  <a class="btn btn-link btn-arrow-right btn-arrow-right-dark btn-arrow-right-hover-primary mt-auto"
                    type="link"
                    href="<?php echo $website_link['url']; ?>">
                    <span class="btn-arrow-text text-uppercase">
                      Website
                      <?php echo file_get_contents($themePath . '/img/arrow-right.svg'); ?> 
                    </span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div> 
<?php
}
wp_reset_query();
?>
      </div>
      <div id="developmentsNavigation" class="slick-navigation mb-6"></div>
    </div>
  </div>
</div>
