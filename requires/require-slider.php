<div class="container-fluid bg-secondary">
  <div class="row d-none d-lg-block">
    <div class="col-48">
      <div class="container container-inner py-3 gutters">
        <div class="row">
          <div class="col col-slide-nav d-flex gutters"> 
<?php 
$query = new WP_Query(array(
  'posts_per_page' => -1,
  'post_type'      => 'developments',
  'order'          => 'DESC',
  'orderby'        => 'menu_order',
  'status'         => 'publish'
));
$count = 0;
while ($query->have_posts()) {
  $query->the_post();
  $slider_image = get_field('slider_image'); 
?> 
            <div class="slide-nav <?php if ( $count === 0 ) { echo 'active'; } ?>" data-slide-number="<?php echo $count; ?>">
              <span class="text-fw-active"><?php echo $slider_image['title']; ?> </span>
              <span class="text-fw-normal"><?php echo $slider_image['title']; ?> </span>
            </div> 
<?php $count++; } wp_reset_query(); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="fp-wrapper" class="row row-fp-hero w-100 bg-dark"> 
<?php 
$query = new WP_Query(array(
  'posts_per_page' => -1,
  'post_type'      => 'developments',
  'order'          => 'DESC',
  'orderby'        => 'menu_order',
  'status'         => 'publish'
));
$count = 0;
while ($query->have_posts()) {
  $query->the_post();
  $logo_white   = get_field('logo_white');
  $slider_image = get_field('slider_image');
  $heading      = get_field('heading');
  $website_link = get_field('website_link');
?> 
    <div class="col-48 fp-slider-col vh-85 d-flex align-items-center justify-content-start"
      style="background-image:url('<?php echo $slider_image['url']; ?>')">
      <div class="container container-inner gutters">
        <div class="sale-sticker d-none d-lg-block"> <?php $sale_sticker = get_field('sale_sticker'); ?>
          <?php if ($sale_sticker) { ?> <img src="<?php echo $sale_sticker['url']; ?>"
            alt="<?php echo $sale_sticker['alt']; ?>" /> <?php } ?></div>
        <div class="row">
          <div class="fp-text gutters">
            <img src="<?php echo $logo_white['url'] ?>" class="logo mb-4">
            <h2 class="slider-heading mb-3"><?php echo $heading; ?></h2> <?php if (get_field('subheading')) { ?>
            <h5 class="slider-subheading mb-5"><?php echo the_field('subheading'); ?></h5> <?php } else { ?> 
            <h5 class="slider-subheading mb-5 text-white fw-300"><?php echo the_field('address_one'); ?>
              <?php echo the_field('address_two'); ?></h5> <?php } ?> 
              <a class="slider-button btn btn-link btn-outline-white btn-hover-primary ls-25 fw-500"
              href="<?php echo $website_link['url']; ?>" <?php echo $website_link['target']; ?>>
              <?php echo $website_link['text']; ?> </a>
          </div>
        </div>
      </div>
    </div> 
<?php $count++; } wp_reset_query(); ?> 
  </div>
</div>
