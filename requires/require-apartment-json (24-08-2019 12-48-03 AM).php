<?php

$apartmentsJSON = false;
$apartmentsArray = false;

//  Initiate curl
$url = 'https://loadeddev.online/apartment-data/?token=3b834b2889800d9e';
$ch = curl_init();
// Will return the response, if false it print the response
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// Set the url
curl_setopt($ch, CURLOPT_URL,$url);
// Execute
$result = curl_exec($ch);
// Closing
curl_close($ch);

$apartmentsJSON = json_encode(json_decode( $result , true));
$apartmentsArray = json_decode( $result , true);
