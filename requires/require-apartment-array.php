<?php

$singleListingArray = array();
$apartmentObject = array();
$site_url = get_bloginfo('url');
$site_url = str_replace('https:', '', $site_url);
$site_url = str_replace('http:', '', $site_url);
$listingsJSON = file_get_contents( get_stylesheet_directory() . '/data/listings.json' );
$listings = json_decode($listingsJSON, true);
$publishedListingsJSON = file_get_contents( get_stylesheet_directory() . '/data/publishedListings.json' );
$publishedListings = json_decode($publishedListingsJSON, true);
foreach( $publishedListings as $listing ) {
  if ( $listing['system_listing_state'] === 'current' ) {
    $property_id = intval($listing['property_id']);
    $building_id = intval($listing['address']['building']['id']);
    $listing_id = $listing['_id'];

    foreach( $listings as $listingItem ) {
      if ( intval($listingItem['property']['id']) === $property_id ) {
        if ( $listingItem['_listing_primary_image'] !== null && array_key_exists( '_url', $listingItem['_listing_primary_image'] ) ) {
          $apartmentObject[$property_id]['primary_image'] = $listingItem['_listing_primary_image']['_url'];
          $apartmentObject[$property_id]['primary_image_sm'] = $listingItem['_listing_primary_image']['_thumbs']['800x600']['_url'];
          $apartmentObject[$property_id]['primary_image_xs'] = $listingItem['_listing_primary_image']['_thumbs']['80x60']['_url'];
        } 
        if ( $listingItem['_listing_primary_image'] === null && $listingItem['property']['_property_image'] !== null && array_key_exists( '_url', $listingItem['property']['_property_image'] ) ) {
          $apartmentObject[$property_id]['primary_image'] = $listingItem['property']['_property_image']['_url'];
          $apartmentObject[$property_id]['primary_image_sm'] = $listingItem['property']['_property_image']['_thumbs']['800x600']['_url'];
          $apartmentObject[$property_id]['primary_image_xs'] = $listingItem['property']['_property_image']['_thumbs']['80x60']['_url'];
        }
        if ( $listingItem['_listing_primary_image'] === null && 
          $listingItem['property']['_property_image'] === null && 
          $listingItem['property']['_adr_building']['_building_image'] !== null && 
          array_key_exists( '_url', $listingItem['property']['_adr_building']['_building_image'] ) ) {
          $apartmentObject[$property_id]['primary_image'] = $listingItem['property']['_adr_building']['_building_image']['_url'];
          $apartmentObject[$property_id]['primary_image_sm'] = $listingItem['property']['_adr_building']['_building_image']['_thumbs']['800x600']['_url'];
          $apartmentObject[$property_id]['primary_image_xs'] = $listingItem['property']['_adr_building']['_building_image']['_thumbs']['80x60']['_url'];
        }
        if ( $listingItem['system_listing_state'] !== null && strval($listingItem['system_listing_state']) > 1 ) {
          $apartmentObject[$property_id]['system_listing_state'] = $listingItem['system_listing_state'];
        }
        if ( $listingItem['system_publication_status'] !== null && strval($listingItem['system_publication_status']) > 1 ) {
          $apartmentObject[$property_id]['system_publication_status'] = $listingItem['system_publication_status'];
        }
        if ( $listingItem['system_has_preupload_errors'] !== null && is_numeric($listingItem['system_has_preupload_errors']) ) {
          $apartmentObject[$property_id]['system_has_preupload_errors'] = $listingItem['system_has_preupload_errors'];
        }
        if ( $listingItem['tenancy_type'] !== null && strval($listingItem['tenancy_type']) > 1 ) {
          $apartmentObject[$property_id]['tenancy_type'] = $listingItem['tenancy_type'];
        }
      }
    }
    if ( array_key_exists($property_id, $apartmentObject) === false ) {
      $apartmentObject[$property_id]['primary_image'] = false;
      $apartmentObject[$property_id]['primary_image_sm'] = false;
    }
    if ( array_key_exists( $property_id, $apartmentObject ) ) {
      $apartmentObject[$property_id]['id'] =                  intval($property_id);
      $apartmentObject[$property_id]['_id'] =                 intval($listing['_id']);
      $apartmentObject[$property_id]['etag'] =                 $listing['etag'];          
      $apartmentObject[$property_id]['property_category'] =   $listing['property_category'];
      $apartmentObject[$property_id]['listing_category_id'] = $listing['listing_category_id'];
      $apartmentObject[$property_id]['price_match'] =         intval($listing['price_match']);
      if (is_numeric($listing['price_advertise_as'])) {
        $apartmentObject[$property_id]['price_advertise_as'] = '$' . number_format(intval($listing['price_advertise_as']));
      } else {
        $apartmentObject[$property_id]['price_advertise_as'] =  strval($listing['price_advertise_as']);
      }
      $apartmentObject[$property_id]['state_value_price_display'] = $listing['state_value_price_display'];
      $apartmentObject[$property_id]['state_hide_price'] = $listing['state_hide_price'];
      $apartmentObject[$property_id]['attributes_bedrooms'] =            $listing['attributes']['bedrooms'];
      $apartmentObject[$property_id]['attributes_bathrooms'] =           $listing['attributes']['bathrooms'];
      $apartmentObject[$property_id]['attributes_ensuites'] =            $listing['attributes']['ensuites'];
      $apartmentObject[$property_id]['attributes_toilets'] =             $listing['attributes']['toilets'];
      $apartmentObject[$property_id]['attributes_living_areas'] =        $listing['attributes']['living_areas'];
      $apartmentObject[$property_id]['attributes_garages'] =             $listing['attributes']['garages'];
      $apartmentObject[$property_id]['attributes_carports'] =            $listing['attributes']['carports'];
      $apartmentObject[$property_id]['attributes_open_spaces'] =         $listing['attributes']['open_spaces'];
      $apartmentObject[$property_id]['attributes_total_car_accom'] =     $listing['attributes']['total_car_accom'];
      $apartmentObject[$property_id]['attributes_buildarea_m2'] =        $listing['attributes']['buildarea_m2'];
      $apartmentObject[$property_id]['attributes_landarea_m2'] =         $listing['attributes']['landarea_m2'];
      $apartmentObject[$property_id]['address_latitude'] =            $listing['address']['latitude'];
      $apartmentObject[$property_id]['address_longitude'] =           $listing['address']['longitude'];
      $apartmentObject[$property_id]['address_unit_number'] =         $listing['address']['unit_number'];
      $apartmentObject[$property_id]['address_street_number'] =       $listing['address']['street_number'];
      $apartmentObject[$property_id]['address_street_name'] =         $listing['address']['street_name'];
      $apartmentObject[$property_id]['address_state_or_region'] =     $listing['address']['state_or_region'];
      $apartmentObject[$property_id]['address_locality'] =            $listing['address']['locality'];
      $apartmentObject[$property_id]['address_suburb_or_town'] =      $listing['address']['suburb_or_town'];
      $apartmentObject[$property_id]['address_postcode'] =            $listing['address']['postcode'];
      $apartmentObject[$property_id]['address_country'] =             $listing['address']['country'];
      $apartmentObject[$property_id]['address_hide_address'] =        $listing['address']['hide_address'];
      $apartmentObject[$property_id]['address_building_name'] =        $listing['address']['building']['name'];
      $apartmentObject[$property_id]['address_building_id'] = $listing['address']['building']['id'];
      $apartmentObject[$property_id]['address_building_building_adr_street_number'] = $listing['address']['building']['adr_street_number'];
      $apartmentObject[$property_id]['address_building_building_adr_street_name'] = $listing['address']['building']['adr_street_name'];
      $apartmentObject[$property_id]['address_building_building_adr_suburb_or_town'] = $listing['address']['building']['adr_suburb_or_town'];
      $apartmentObject[$property_id]['address_building_building_adr_state_or_region'] = $listing['address']['building']['adr_state_or_region'];
      $apartmentObject[$property_id]['address_building_building_adr_locality'] = $listing['address']['building']['adr_locality'];
      $apartmentObject[$property_id]['address_building_building_adr_postcode'] = $listing['address']['building']['adr_postcode'];
      $apartmentObject[$property_id]['address_building_building_adr_country'] = $listing['address']['building']['adr_country'];
      if ( $listing['address']['building']['building_image'] !== null && array_key_exists( 'url', $listing['address']['building']['building_image'] ) ) {
        $apartmentObject[$property_id]['address_building_building_image'] = $listing['address']['building']['building_image']['url'];
      }
      $apartmentObject[$property_id]['address_formats_street_name_number'] = $listing['address']['formats']['street_name_number'];
      $apartmentObject[$property_id]['address_formats_street_name_number_w_suburb'] = $listing['address']['formats']['street_name_number_w_suburb'];
      $apartmentObject[$property_id]['address_formats_full_address'] = $listing['address']['formats']['full_address'];
      $apartmentObject[$property_id]['address_formats_full_address_w_building_name'] = $listing['address']['formats']['full_address_w_building_name'];
      $apartmentObject[$property_id]['address_formats_hidden_address'] = $listing['address']['formats']['hidden_address'];
      $apartmentObject[$property_id]['address_formats_display_address'] = $listing['address']['formats']['display_address'];
      $apartmentObject[$property_id]['listing_agent_1_id'] = $listing['listing_agent_1']['id'];
      $apartmentObject[$property_id]['listing_agent_1_name'] = $listing['listing_agent_1']['name'];
      $apartmentObject[$property_id]['listing_agent_1_first_name'] = $listing['listing_agent_1']['first_name'];
      $apartmentObject[$property_id]['listing_agent_1_last_name'] = $listing['listing_agent_1']['last_name'];
      $apartmentObject[$property_id]['listing_agent_1_email_address'] = $listing['listing_agent_1']['email_address'];
      $apartmentObject[$property_id]['listing_agent_1_phone_direct'] = $listing['listing_agent_1']['phone_direct'];
      $apartmentObject[$property_id]['listing_agent_1_phone_mobile'] = $listing['listing_agent_1']['phone_mobile'];
      $apartmentObject[$property_id]['listing_agent_1_position'] = $listing['listing_agent_1']['position'];
      $apartmentObject[$property_id]['listing_agent_1_is_account_user'] = $listing['listing_agent_1']['is_account_user'];
      if ( $listing['listing_agent_1']['profile_image'] !== null && array_key_exists( 'url', $listing['listing_agent_1']['profile_image'] ) ) {
        $apartmentObject[$property_id]['listing_agent_1_profile_image_url'] = $listing['listing_agent_1']['profile_image']['url'];
      }
      $apartmentObject[$property_id]['contact_status'] = $listing['contract']['status'];
      $apartmentObject[$property_id]['account_id'] = intval($listing['account']['id']);
      $apartmentObject[$property_id]['account_name'] = $listing['account']['name'];
      $apartmentObject[$property_id]['account_country_id'] = $listing['account']['country_id'];
      $apartmentObject[$property_id]['account_country_region_id'] = $listing['account']['country_region_id'];
      $apartmentObject[$property_id]['account_timezone_id'] = $listing['account']['timezone_id'];
      if ( $listing['location']['id'] !== null ) {
        $apartmentObject[$property_id]['location_id'] = intval($listing['location']['id']);
        $apartmentObject[$property_id]['location_location_name'] = $listing['location']['location_name'];
        $apartmentObject[$property_id]['location_website'] = $listing['location']['website'];
        $apartmentObject[$property_id]['location_email'] = $listing['location']['email'];
        $apartmentObject[$property_id]['location_phone'] = $listing['location']['phone'];
        $apartmentObject[$property_id]['location_fax'] = $listing['location']['fax'];
        $apartmentObject[$property_id]['location_address_physical'] = $listing['location']['address_physical'];
        $apartmentObject[$property_id]['location_report_color'] = $listing['location']['report_color'];
        if ( $listing['location']['logo_image'] !== null && array_key_exists( 'url', $listing['location']['logo_image'] ) ) {
          $apartmentObject[$property_id]['location_logo_image_url'] = $listing['location']['logo_image']['url'];
        }
      }
      
      if ( $listing['address']['building']['id'] !== null && $building_id > 0 ) {
        $query = new WP_Query(array(
            'post_type' => 'developments',
            'post_status' => 'publish',
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'posts_per_page' => -1
        ));
        while ($query->have_posts()) {
            $query->the_post();
            $development_id = get_the_ID();
            if ( get_field( 'rex_building_id', $development_id ) ) {
              $rex_building_id = get_field( 'rex_building_id', $development_id );
              if ( intval($building_id) === intval($rex_building_id) ) {
                $development_title = get_the_title( $development_id );
                $development_content = apply_filters( 'the_content', get_the_content($development_id) );
                $development_permalink = get_the_permalink($development_id);
                $apartmentObject[$property_id]['rex_building_id'] = intval($rex_building_id);
                $apartmentObject[$property_id]['development_post_id'] = $development_id;
                $apartmentObject[$property_id]['development_title'] = $development_title;
                $apartmentObject[$property_id]['development_content'] = $development_content;
                $apartmentObject[$property_id]['development_permalink'] = $development_permalink;
                if ( get_field('logo_white', $development_id) ) { 
                  $development_logo_white = get_field('logo_white', $development_id);
                  $development_logo_white = $development_logo_white['url'];
                  $apartmentObject[$property_id]['development_logo_white'] = $site_url . $development_logo_white;
                }
                if ( get_field('logo_black', $development_id) ) { 
                  $development_logo_black = get_field('logo_black', $development_id);
                  $development_logo_black = $development_logo_black['url'];
                  $apartmentObject[$property_id]['development_logo_black'] = $site_url . $development_logo_black;
                }
                
                $apartmentObject[$property_id]['development_short_name'] = get_field('short_name', $development_id);
                $apartmentObject[$property_id]['development_from_price'] = get_field('from_price', $development_id);
                $apartmentObject[$property_id]['development_description'] = get_field('description', $development_id);
                $apartmentObject[$property_id]['development_short_description'] = get_field('short_description', $development_id);
                $apartmentObject[$property_id]['development_website_link'] = get_field('website_link', $development_id);
                $apartmentObject[$property_id]['development_apartments_link'] = get_field('apartments_link', $development_id);
                $apartmentObject[$property_id]['development_address_one'] = get_field('address_one', $development_id);
                $apartmentObject[$property_id]['development_address_two'] = get_field('address_two', $development_id);
                if ( get_field('slider_image', $development_id) ) { 
                  $development_slider_image = get_field('slider_image', $development_id);
                  $development_slider_image = $development_slider_image['url'];
                  $apartmentObject[$property_id]['development_slider_image'] = $site_url . $development_slider_image;
                }
                if ( get_field('sale_sticker', $development_id) ) { 
                  $development_sale_sticker = get_field('sale_sticker', $development_id);
                  $development_sale_sticker = $development_sale_sticker['url'];
                  $apartmentObject[$property_id]['development_sale_sticker'] = $site_url . $development_sale_sticker;
                }
                $apartmentObject[$property_id]['development_heading'] = get_field('heading', $development_id);
                $apartmentObject[$property_id]['development_subheading'] = get_field('subheading', $development_id);
                if ( get_field('our_developments_image', $development_id) ) { 
                  $development_our_developments_image = get_field('our_developments_image', $development_id);
                  $development_our_developments_image = $development_our_developments_image['url'];
                  $apartmentObject[$property_id]['development_our_developments_image'] = $site_url . $development_our_developments_image;
                }
                $apartmentObject[$property_id]['development_opening_title'] = get_field('opening_title', $development_id);
                $apartmentObject[$property_id]['development_open_hours'] = get_field('open_hours', $development_id);
                $apartmentObject[$property_id]['development_contact_agent_name'] = get_field('contact_agent_name', $development_id);
                $apartmentObject[$property_id]['development_contact_agent_number'] = get_field('contact_agent_number', $development_id);
                $apartmentObject[$property_id]['development_contact_agent_dial'] = get_field('contact_agent_dial', $development_id);
                $apartmentObject[$property_id]['development_google_map'] = get_field('google_map', $development_id);
                if ( get_field('pdf_floor_plans', $development_id ) ) {
                  $apartmentObject[$property_id]['development_pdf_floor_plans'] = $site_url . get_field('pdf_floor_plans', $development_id);
                } 
                if ( get_field('pdf_floor_plans', $development_id ) === false ) {
                  $apartmentObject[$property_id]['development_pdf_floor_plans'] = false;
                }
                if ( get_field('pdf_plans', $development_id ) ) {
                  $apartmentObject[$property_id]['development_pdf_plans'] = $site_url . get_field('pdf_plans', $development_id);
                }
                if ( get_field('pdf_plans', $development_id ) === false ) {
                  $apartmentObject[$property_id]['development_pdf_plans'] = false;
                }
                if ( get_field('pdf_specifications', $development_id ) ) {
                  $apartmentObject[$property_id]['development_pdf_specifications'] = $site_url . get_field('pdf_specifications', $development_id);
                } 
                if ( get_field('pdf_specifications', $development_id ) === false ) {
                  $apartmentObject[$property_id]['development_pdf_specifications'] = false;
                }
              }
            }
        }
        wp_reset_query();
      }
    }
  }
}
$apartmentArray = array();
foreach ( $apartmentObject as $object ) {
  $apartmentArray[] = $object;
}