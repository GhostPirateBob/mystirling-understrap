<div id="apartmentFilters" class="wrapper">
  <div class="row bg-dark align-items-center">
    <div class="col-48">
      <div class="row row-apartment-search">
        <div class="col form-group filter-apartment"><label for="#priceMinSelected" class="label-filter label-filter-apartment"> Price
            <small>(min)</small></label><span class="deco deco-triangle-up"></span>
          <div class="wrapper wrapper-developments-select">
            <div dir="auto" class="v-select vs--single vs--searchable" id="priceMinSelected">
              <div class="vs__dropdown-toggle">
                <div class="vs__selected-options"><span class="vs__selected">
                    Any
                    <!----></span> <input aria-label="Search for option" role="combobox" type="search" autocomplete="off" class="vs__search"></div>
                <div class="vs__actions"><button type="button" title="Clear selection" class="vs__clear"><svg xmlns="http://www.w3.org/2000/svg" width="10"
                      height="10">
                      <path
                        d="M6.895455 5l2.842897-2.842898c.348864-.348863.348864-.914488 0-1.263636L9.106534.261648c-.348864-.348864-.914489-.348864-1.263636 0L5 3.104545 2.157102.261648c-.348863-.348864-.914488-.348864-1.263636 0L.261648.893466c-.348864.348864-.348864.914489 0 1.263636L3.104545 5 .261648 7.842898c-.348864.348863-.348864.914488 0 1.263636l.631818.631818c.348864.348864.914773.348864 1.263636 0L5 6.895455l2.842898 2.842897c.348863.348864.914772.348864 1.263636 0l.631818-.631818c.348864-.348864.348864-.914489 0-1.263636L6.895455 5z">
                      </path>
                    </svg></button> <svg xmlns="http://www.w3.org/2000/svg" width="14" height="10" role="presentation" class="vs__open-indicator">
                    <path
                      d="M9.211364 7.59931l4.48338-4.867229c.407008-.441854.407008-1.158247 0-1.60046l-.73712-.80023c-.407008-.441854-1.066904-.441854-1.474243 0L7 5.198617 2.51662.33139c-.407008-.441853-1.066904-.441853-1.474243 0l-.737121.80023c-.407008.441854-.407008 1.158248 0 1.600461l4.48338 4.867228L7 10l2.211364-2.40069z">
                    </path>
                  </svg>
                  <div class="vs__spinner" style="display: none;">Loading...</div>
                </div>
              </div>
              <!---->
            </div>
          </div>
        </div>
        <div class="col form-group filter-apartment"><label for="#priceMaxSelected" class="label-filter label-filter-apartment"> Price
            <small>(max)</small></label><span class="deco deco-triangle-up"></span>
          <div class="wrapper wrapper-developments-select">
            <div dir="auto" class="v-select vs--single vs--searchable" id="priceMaxSelected">
              <div class="vs__dropdown-toggle">
                <div class="vs__selected-options"><span class="vs__selected">
                    Any
                    <!----></span> <input aria-label="Search for option" role="combobox" type="search" autocomplete="off" class="vs__search"></div>
                <div class="vs__actions"><button type="button" title="Clear selection" class="vs__clear"><svg xmlns="http://www.w3.org/2000/svg" width="10"
                      height="10">
                      <path
                        d="M6.895455 5l2.842897-2.842898c.348864-.348863.348864-.914488 0-1.263636L9.106534.261648c-.348864-.348864-.914489-.348864-1.263636 0L5 3.104545 2.157102.261648c-.348863-.348864-.914488-.348864-1.263636 0L.261648.893466c-.348864.348864-.348864.914489 0 1.263636L3.104545 5 .261648 7.842898c-.348864.348863-.348864.914488 0 1.263636l.631818.631818c.348864.348864.914773.348864 1.263636 0L5 6.895455l2.842898 2.842897c.348863.348864.914772.348864 1.263636 0l.631818-.631818c.348864-.348864.348864-.914489 0-1.263636L6.895455 5z">
                      </path>
                    </svg></button> <svg xmlns="http://www.w3.org/2000/svg" width="14" height="10" role="presentation" class="vs__open-indicator">
                    <path
                      d="M9.211364 7.59931l4.48338-4.867229c.407008-.441854.407008-1.158247 0-1.60046l-.73712-.80023c-.407008-.441854-1.066904-.441854-1.474243 0L7 5.198617 2.51662.33139c-.407008-.441853-1.066904-.441853-1.474243 0l-.737121.80023c-.407008.441854-.407008 1.158248 0 1.600461l4.48338 4.867228L7 10l2.211364-2.40069z">
                    </path>
                  </svg>
                  <div class="vs__spinner" style="display: none;">Loading...</div>
                </div>
              </div>
              <!---->
            </div>
          </div>
        </div>
        <div class="col form-group filter-apartment"><label for="#bedroomsSelected" class="label-filter label-filter-apartment"> Bedrooms
            <small>(min)</small></label><span class="deco deco-triangle-up"></span>
          <div class="wrapper wrapper-developments-select">
            <div dir="auto" class="v-select vs--single vs--searchable" id="bedroomsSelected">
              <div class="vs__dropdown-toggle">
                <div class="vs__selected-options"><span class="vs__selected">
                    Any
                    <!----></span> <input aria-label="Search for option" role="combobox" type="search" autocomplete="off" class="vs__search"></div>
                <div class="vs__actions"><button type="button" title="Clear selection" class="vs__clear"><svg xmlns="http://www.w3.org/2000/svg" width="10"
                      height="10">
                      <path
                        d="M6.895455 5l2.842897-2.842898c.348864-.348863.348864-.914488 0-1.263636L9.106534.261648c-.348864-.348864-.914489-.348864-1.263636 0L5 3.104545 2.157102.261648c-.348863-.348864-.914488-.348864-1.263636 0L.261648.893466c-.348864.348864-.348864.914489 0 1.263636L3.104545 5 .261648 7.842898c-.348864.348863-.348864.914488 0 1.263636l.631818.631818c.348864.348864.914773.348864 1.263636 0L5 6.895455l2.842898 2.842897c.348863.348864.914772.348864 1.263636 0l.631818-.631818c.348864-.348864.348864-.914489 0-1.263636L6.895455 5z">
                      </path>
                    </svg></button> <svg xmlns="http://www.w3.org/2000/svg" width="14" height="10" role="presentation" class="vs__open-indicator">
                    <path
                      d="M9.211364 7.59931l4.48338-4.867229c.407008-.441854.407008-1.158247 0-1.60046l-.73712-.80023c-.407008-.441854-1.066904-.441854-1.474243 0L7 5.198617 2.51662.33139c-.407008-.441853-1.066904-.441853-1.474243 0l-.737121.80023c-.407008.441854-.407008 1.158248 0 1.600461l4.48338 4.867228L7 10l2.211364-2.40069z">
                    </path>
                  </svg>
                  <div class="vs__spinner" style="display: none;">Loading...</div>
                </div>
              </div>
              <!---->
            </div>
          </div>
        </div>
        <div class="col form-group filter-apartment"><label for="#developmentSelected" class="label-filter label-filter-apartment"> Development </label><span
            class="deco deco-triangle-up"></span>
          <div class="wrapper wrapper-developments-select">
            <div dir="auto" class="v-select vs--single vs--searchable" id="developmentSelected">
              <div class="vs__dropdown-toggle">
                <div class="vs__selected-options"><span class="vs__selected">
                    Any
                    <!----></span> <input aria-label="Search for option" role="combobox" type="search" autocomplete="off" class="vs__search"></div>
                <div class="vs__actions"><button type="button" title="Clear selection" class="vs__clear"><svg xmlns="http://www.w3.org/2000/svg" width="10"
                      height="10">
                      <path
                        d="M6.895455 5l2.842897-2.842898c.348864-.348863.348864-.914488 0-1.263636L9.106534.261648c-.348864-.348864-.914489-.348864-1.263636 0L5 3.104545 2.157102.261648c-.348863-.348864-.914488-.348864-1.263636 0L.261648.893466c-.348864.348864-.348864.914489 0 1.263636L3.104545 5 .261648 7.842898c-.348864.348863-.348864.914488 0 1.263636l.631818.631818c.348864.348864.914773.348864 1.263636 0L5 6.895455l2.842898 2.842897c.348863.348864.914772.348864 1.263636 0l.631818-.631818c.348864-.348864.348864-.914489 0-1.263636L6.895455 5z">
                      </path>
                    </svg></button> <svg xmlns="http://www.w3.org/2000/svg" width="14" height="10" role="presentation" class="vs__open-indicator">
                    <path
                      d="M9.211364 7.59931l4.48338-4.867229c.407008-.441854.407008-1.158247 0-1.60046l-.73712-.80023c-.407008-.441854-1.066904-.441854-1.474243 0L7 5.198617 2.51662.33139c-.407008-.441853-1.066904-.441853-1.474243 0l-.737121.80023c-.407008.441854-.407008 1.158248 0 1.600461l4.48338 4.867228L7 10l2.211364-2.40069z">
                    </path>
                  </svg>
                  <div class="vs__spinner" style="display: none;">Loading...</div>
                </div>
              </div>
              <!---->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
