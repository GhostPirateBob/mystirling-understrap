<?php 

require_once( get_stylesheet_directory() . '/vendor/autoload.php');

$apiOptions = array();
$apiHeaders = array();

$apiEndpoint = "https://api.rexsoftware.com/rex.php";
$apiHeaders[] = "content-type: application/json";

$postData = '{
  "method":"Listings::read",
  "args":{
    "id": 1342625,
    "extra_fields": ["core", "property_core", "property", "_related.property_features", "_related.property_views", "_related_property_tags", "_related.contact_reln_listing", "_related.listing_idealfors", "_related.listing_allowances", "_related.listing_adverts", "_related.listing_events", "_related.listing_images", "_related.listing_documents", "_related.listing_floorplans", "_related.listing_holidaybookings", "_related.listing_links", "_related.listing_highlights", "_related.listing_rooms"]
  },
  "token": "5ff0-86c3-072b-b8fc-932e-3bb5-e7bb-5cc8"
 }
';

$tokenData = '{
  "method":"Authentication::login",
  "args":{
  "email":"aimee@stirlingcapital.com.au",
  "password":"Rextesting2018",
  "application":"rex"
  }
 }
';

$publishedListingsData = '{';


$apiToken = false;

$tokenRequest = Requests::post( $apiEndpoint, $apiHeaders, $tokenData, $apiOptions );

if ( $tokenRequest->status_code === 200 && isJSON($tokenRequest->body) ) {
  $tokenJSON = json_decode($tokenRequest->body, true);
  if ( $tokenJSON['error'] === NULL && strlen($tokenJSON['result']) > 32 ) {
    file_put_contents( get_stylesheet_directory() . '/data/token.json', json_encode($tokenJSON['result']));
    $apiToken = $tokenJSON['result'];
  }
}

if ( $apiToken ) {

$publishedListingsData = '{
    "method": "PublishedListings::search",
    "args": {
      "limit": 100
    },
    "token": "' . $apiToken .'"
  }
';

$publishedListingsRequest = Requests::post( $apiEndpoint, $apiHeaders, $publishedListingsData, $apiOptions );
$publishedListingsJSON = json_decode($publishedListingsRequest->body, true);

if ( is_array($publishedListingsJSON) && array_key_exists('result', $publishedListingsJSON ) ) {
  $publishedListingsJSON = json_encode($publishedListingsJSON['result']['rows']);
  file_put_contents( get_stylesheet_directory() . '/data/publisedListings.json', $publishedListingsJSON );
}

echo ' <br> <pre style="display: block;width: 1200px;max-width: 100%;background: #fbfbfb;color: #000;margin: 16px auto 32px !important;border-radius: 5px;border: 2px solid #f7acac;padding: 4px 16px !important;font-size: 1.125rem;white-space: pre-wrap;word-break: break-all;text-overflow: ellipsis;overflow-x: hidden;overflow-y: auto;box-shadow: 0 3px 5px -1px rgba(0,0,0,.2), 0 5px 8px 0 rgba(0,0,0,.14), 0 1px 14px 0 rgba(0,0,0,.12);">'; highlight_string("<?php data =\n\n" . var_export( $publishedListingsJSON , true ) . "; ?>"); echo ' </pre> <br> <br> ';

$listingsData = '{
  "method": "Listings::search",
  "args": {
    "limit": 100
  },
  "token": "' . $apiToken .'"
}
';

$listingsRequest = Requests::post( $apiEndpoint, $apiHeaders, $listingsData, $apiOptions );
$listingsJSON = json_decode($listingsRequest->body, true);

if ( is_array($listingsJSON) && array_key_exists('result', $listingsJSON ) ) {
  $listingsJSON = json_encode($listingsJSON['result']['rows']);
  file_put_contents( get_stylesheet_directory() . '/data/listings.json', $listingsJSON );
}

echo ' <br> <pre style="display: block;width: 1200px;max-width: 100%;background: #fbfbfb;color: #000;margin: 16px auto 32px !important;border-radius: 5px;border: 2px solid #f7acac;padding: 4px 16px !important;font-size: 1.125rem;white-space: pre-wrap;word-break: break-all;text-overflow: ellipsis;overflow-x: hidden;overflow-y: auto;box-shadow: 0 3px 5px -1px rgba(0,0,0,.2), 0 5px 8px 0 rgba(0,0,0,.14), 0 1px 14px 0 rgba(0,0,0,.12);">'; highlight_string("<?php data =\n\n" . var_export( $listingsJSON , true ) . "; ?>"); echo ' </pre> <br> <br> ';

$buildingsData = '{
  "method": "Buildings::search",
  "args": {
    "limit": 100
  },
  "token": "' . $apiToken .'"
}
';

$buildingsRequest = Requests::post( $apiEndpoint, $apiHeaders, $buildingsData, $apiOptions );
$buildingsJSON = json_decode($buildingsRequest->body, true);

if ( is_array($buildingsJSON) && array_key_exists('result', $buildingsJSON ) ) {
  $buildingsJSON = json_encode($buildingsJSON['result']['rows']);
  file_put_contents( get_stylesheet_directory() . '/data/buildings.json', $buildingsJSON );
}

echo ' <br> <pre style="display: block;width: 1200px;max-width: 100%;background: #fbfbfb;color: #000;margin: 16px auto 32px !important;border-radius: 5px;border: 2px solid #f7acac;padding: 4px 16px !important;font-size: 1.125rem;white-space: pre-wrap;word-break: break-all;text-overflow: ellipsis;overflow-x: hidden;overflow-y: auto;box-shadow: 0 3px 5px -1px rgba(0,0,0,.2), 0 5px 8px 0 rgba(0,0,0,.14), 0 1px 14px 0 rgba(0,0,0,.12);">'; highlight_string("<?php data =\n\n" . var_export( $buildingsJSON , true ) . "; ?>"); echo ' </pre> <br> <br> ';

$projectsData = '{
  "method": "Projects::search",
  "args": {
    "limit": 100
  },
  "token": "' . $apiToken .'"
}
';

$projectsRequest = Requests::post( $apiEndpoint, $apiHeaders, $projectsData, $apiOptions );
$projectsJSON = json_decode($projectsRequest->body, true);

if ( is_array($projectsJSON) && array_key_exists('result', $projectsJSON ) ) {
  $projectsJSON = json_encode($projectsJSON['result']['rows']);
  file_put_contents( get_stylesheet_directory() . '/data/projects.json', $projectsJSON );
}

echo ' <br> <pre style="display: block;width: 1200px;max-width: 100%;background: #fbfbfb;color: #000;margin: 16px auto 32px !important;border-radius: 5px;border: 2px solid #f7acac;padding: 4px 16px !important;font-size: 1.125rem;white-space: pre-wrap;word-break: break-all;text-overflow: ellipsis;overflow-x: hidden;overflow-y: auto;box-shadow: 0 3px 5px -1px rgba(0,0,0,.2), 0 5px 8px 0 rgba(0,0,0,.14), 0 1px 14px 0 rgba(0,0,0,.12);">'; highlight_string("<?php data =\n\n" . var_export( $projectsJSON , true ) . "; ?>"); echo ' </pre> <br> <br> ';

$propertiesData = '{
  "method": "Properties::search",
  "args": {
    "limit": 100
  },
  "token": "' . $apiToken .'"
}
';

$propertiesRequest = Requests::post( $apiEndpoint, $apiHeaders, $propertiesData, $apiOptions );
$propertiesJSON = json_decode($propertiesRequest->body, true);

if ( is_array($propertiesJSON) && array_key_exists('result', $propertiesJSON ) ) {
  $propertiesJSON = json_encode($propertiesJSON['result']['rows']);
  file_put_contents( get_stylesheet_directory() . '/data/properties.json', $propertiesJSON );
}

echo ' <br> <pre style="display: block;width: 1200px;max-width: 100%;background: #fbfbfb;color: #000;margin: 16px auto 32px !important;border-radius: 5px;border: 2px solid #f7acac;padding: 4px 16px !important;font-size: 1.125rem;white-space: pre-wrap;word-break: break-all;text-overflow: ellipsis;overflow-x: hidden;overflow-y: auto;box-shadow: 0 3px 5px -1px rgba(0,0,0,.2), 0 5px 8px 0 rgba(0,0,0,.14), 0 1px 14px 0 rgba(0,0,0,.12);">'; highlight_string("<?php data =\n\n" . var_export( $propertiesJSON , true ) . "; ?>"); echo ' </pre> <br> <br> ';

}

$fetchListings = json_decode($listingsJSON, true);

foreach ( $fetchListings as $key => $listing ) {
  $listingData = '{
  "method": "Listings::read",
  "args": {
    "id": ' . strval($listing['_id']) . ',
    "extra_options": {
      "fields": ["property", "_related.property_features", "_related.property_views", "_related_property_tags", "_related.contact_reln_listing", "_related.listing_idealfors", "_related.listing_allowances", "_related.listing_adverts", "_related.listing_events", "_related.listing_images", "_related.listing_documents", "_related.listing_floorplans", "_related.listing_holidaybookings", "_related.listing_links", "_related.listing_highlights", "_related.listing_rooms"]
    }
  },
  "token": "' . $apiToken .'"
}
';
  
  $listingRequest = Requests::post( $apiEndpoint, $apiHeaders, $listingData, $apiOptions );
  $listingJSON = json_decode($listingRequest->body, true);

  if ( is_array($listingJSON) && array_key_exists('result', $listingJSON ) ) {
    $listingJSON = json_encode($listingJSON['result']);
    file_put_contents( get_stylesheet_directory() . '/data/listings/'.$listing['_id'].'.json', $listingJSON );
  }

  echo ' <br> <pre style="display: block;width: 1200px;max-width: 100%;background: #fbfbfb;color: #000;margin: 16px auto 32px !important;border-radius: 5px;border: 2px solid #f7acac;padding: 4px 16px !important;font-size: 1.125rem;white-space: pre-wrap;word-break: break-all;text-overflow: ellipsis;overflow-x: hidden;overflow-y: auto;box-shadow: 0 3px 5px -1px rgba(0,0,0,.2), 0 5px 8px 0 rgba(0,0,0,.14), 0 1px 14px 0 rgba(0,0,0,.12);">'; highlight_string("<?php data =\n\n" . var_export( $listingJSON , true ) . "; ?>"); echo ' </pre> <br> <br> ';
}
