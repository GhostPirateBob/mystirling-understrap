<div class="container-fluid bg-light">
  <div class="row">
    <div class="container insightsContainer pt-lg-6">
      <div class="row mb-5">
        <div class="container container-inner">
          <div class="row pt-4 pt-lg-0">
            <div class="col-48 gutters" data-aos="fade-up">
              <h2>Insights</h2>
            </div>
          </div>
        </div>
      </div>
      <div id="insightsSlider" class="row " data-aos="fade-up">
      
<?php $query = new WP_Query(array(
  'posts_per_page' => 12,
  'post_type'      => 'insights',
  'order'          => 'DESC',
  'post_status'    => 'publish'
));
while ($query->have_posts()) {
  $query->the_post();
  $title          = get_the_title();
  $insights_post_image = get_field( 'insights_post_image' );
?>
  <div class="card-deck d-flex">
        <a class="card gutters" href="<?php the_permalink(); ?>">
          <img class="card-img-top" src="<?php echo  $insights_post_image['url']; ?>">
          <div class="card-body">
            <h3 class="insights-title mb-0"><?php the_title(); ?></h3>
          </div>
          <div class="card-spacer"></div>
          <div class="card-footer">
            <span class="btn btn-link btn-arrow-right btn-arrow-right-dark btn-arrow-right-hover-primary" href="<?php the_permalink(); ?>" role="button">
        <span class="btn-arrow-text">  Read More </span>
        <?php echo file_get_contents($themePath . '/img/arrow-right-dark.svg'); ?> 
</span>
          </div>
        </a> 
        </div>
<?php } wp_reset_query(); ?> 

      </div>
      <div id="insightsNavigation" class="slick-navigation mb-6"></div>
    </div>
  </div>
</div>
