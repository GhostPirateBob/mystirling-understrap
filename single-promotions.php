<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}

get_header();

$siteURL = get_bloginfo('url');
$themeURL  = get_stylesheet_directory_uri();
$siteTitle = get_bloginfo('Title');
$themePath = get_stylesheet_directory();
$promo_image = get_field('promo_image');
$promo_text = get_field('promo_text');
$promotion_intro = get_field('promotion_intro');
$gallery_images = get_field( 'gallery' ); 
$development_logo = get_field ('development_logo');
if ($development_logo) {
$development_logo_id = $development_logo->ID;
$logo_black = get_field('logo_black', $development_logo_id);
}
$promotion_description = get_field ('promotion_description');
?>

<div class="wrapper" id="single-wrapper">
  <div class="container-fluid" id="content" tabindex="-1">
    <div class="row">
      <div class="col content-area bg-light" id="primary">
        <div class="entry-content">
          <div class="container-fluid mb-5">
            <div class="subpage-banner row">
              <img src="<?php echo $promo_image['url'];?>" class="img-fit">
            </div>
          </div>
          <div class="container mb-0 mb-lg-5 gutters">
            <div class="row mb-3 mb-lg-5">
              <div class="col-48 col-lg-42 mx-auto d-flex flex-column flex-md-row gutters justify-content-between align-items-lg-center align-items-start ">
              <h5 class="fw-400 ls-0 mw-lg-30 order-lg-1 order-2"><?php echo strip_tags($promotion_intro);?></h5>
              <?php if (!$development_logo) 
                 { $logo = get_field('logo', 'option');?>
              <?php loadedImage($logo, ['d-inline-block', 'align-top', 'img-fluid', 'order-lg-2', 'order-1', 'mb-lg-0', 'mb-4']); ?>
              <?php } else { ?>
                <img class="order-lg-1 order-2" src="<?php echo $logo_black['url'];?>"/>
                <?php } ?>
              </div>
            </div>
            <div class="row">
              <div class="col-48 col-lg-30 mx-auto mb-0 mb-lg-5 gutters">
                <?php echo ($promotion_description);?>
              </div>
              <div class="col-48 col-lg-30 mx-auto gutters">
<?php
if ( $gallery_images ) {
  foreach ( $gallery_images as $gallery_image ) { ?>
                  <img src="<?php echo $gallery_image['url']; ?>" />
<?php
  }
}
?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> 
  </div>
</div><?php // need this extra closing tag ?>
</div>
<?php get_footer();
