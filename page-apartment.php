<?php
/**
 * Template Name: Single Apartment
 * @package mystirling
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly.
}

get_header();
$themePath = get_stylesheet_directory();

if (file_exists( get_stylesheet_directory() . '/requires/require-apartment-json.php' )) {
  require_once( get_stylesheet_directory() . '/requires/require-apartment-json.php' );
}

if ( isset( $_REQUEST ) && is_array( $_REQUEST ) && array_key_exists('apartment', $_REQUEST ) ) {
  $apartmentID = $_REQUEST['apartment'];
} else {
  echo '<div class="container container-inner debug-pre-wrapper" style="display: none;"><h3 class="text-danger mt-5 mb-5">ERROR APARTMENT ID NOT FOUND</h3></div>';
  $apartmentID = false;
}

if ( $apartmentID ) {
  foreach ($apartmentsArray as $apartment) {
    if ( intval($apartmentID) === intval($apartment['_id']) ) {
      $apartmentAltID = $apartment['id'];
    }
  }
}

$listingObject = false;
if ( file_exists( get_stylesheet_directory() . '/data/listings/'.strval($apartmentID).'.json') ) {
  $listingObject = json_decode(file_get_contents( get_stylesheet_directory() . '/data/listings/'.strval($apartmentID).'.json' ), true);
}

?>

<div class="wrapper" id="page-wrapper">
  <div class="container-fluid" id="content" tabindex="-1">
    <div class="row row-entry-content">
      <div class="col-md-48 content-area entry-content" id="primary">
        <div class="container">
          <div id="apartmentsSlider" class="row row-apartment-slider slider-arrows-inline mt-5 mb-5">
            <div class="col-48">
            <img src="<?php echo $apartmentsArray[$apartmentAltID]['primary_image']; ?>" class="img-fluid" />
          </div>
        </div>
      </div>

      <div class="container-fluid">
        <div class="container">
          <div class="row">
            <div class="col-auto page-title bg-primary dp-00 z-10" data-aos="fade-up">
              <h2 class="text-dark mb-3"> 
                <?php echo $apartmentsArray[$apartmentAltID]['address_building_name']; ?>
              </h2>
              <h3 class="text-dark mb-0"> 
                Apt <?php echo $apartmentsArray[$apartmentAltID]['address_unit_number']; ?>, <br />
                <?php echo $apartmentsArray[$apartmentAltID]['address_street_number']; ?> <?php echo $apartmentsArray[$apartmentAltID]['address_street_name']; ?>, <br />
                <?php echo $apartmentsArray[$apartmentAltID]['address_suburb_or_town']; ?>
              </h3>
            </div>
          </div>
        </div>

        <div class="wrapper wrapper-apartment-block bg-light mt-neg-8 pt-7 pb-7">
          <div class="container gutters container-inner">
            <div class="row mt-7">
              <div class="col-48 mb-3">
                <h3 class="text-primary d-inline-block"> 
<?php
if ( $apartmentsArray[$apartmentAltID]['price_match']) {
  setlocale(LC_MONETARY,"en_AU");
  echo '&dollar;' . number_format($apartmentsArray[$apartmentAltID]['price_match']);
}
?>
          </h3>
          <span class="d-inline-block ml-0 ml-sm-5">
          <?php echo $apartmentsArray[$apartmentAltID]['attributes_bedrooms']; ?> bed
            &#65291; <?php echo $apartmentsArray[$apartmentAltID]['attributes_bathrooms']; ?> bath
            <?php if ( $apartmentsArray[$apartmentAltID]['attributes_garages'] ) { ?>
              &#65291; <?php echo $apartmentsArray[$apartmentAltID]['attributes_garages']; ?> carbay
            <?php } ?>
          </span>
        </div>
        <div class="col-48 mb-5">
<?php 
if ( $listingObject && array_key_exists( '_related', $listingObject ) ) { 
  if ( is_array($listingObject['_related']['listing_adverts'][0]) && array_key_exists( 'advert_body', $listingObject['_related']['listing_adverts'][0] ) ) { 
    echo wpautop($listingObject['_related']['listing_adverts'][0]['advert_body']);
  }
}
?>
        </div>
        <div class="col-48">
          <a href="#footer-contact" type="button" class="btn btn-sm btn-outline-dark p-3 mr-3 mb-2 mt-2 mt-sm-0 mb-sm-0"> Request Viewing </a>
          <a href="#footer-contact" type="button" class="btn btn-sm btn-outline-dark p-3 mb-2 mt-2 mt-sm-0 mb-sm-0"> Enquire </a>
        </div>
      </div>
    </div>
  </div>

  <div class="wrapper wrapper-apartment-features">
    <div class="container container-inner">
      <div class="row align-items-start">
        <div class="col-48 col-lg-22 gutters pt-5 pb-5 pt-lg-7 pb-lg-7 order-3 order-md-1">
          <div class="row">
          <?php if ( $listingObject && array_key_exists('_related', $listingObject) && count($listingObject['_related']['property_features']) >= 1 ) { ?>
            <div class="col-48 mb-5">
              <h3> Apartment Features </h3>
              <hr class="mb-4" />
              <ul class="list-unstyled lh-20">
              <?php foreach ( $listingObject['_related']['property_features'] as $feature ) { ?>
                <li><?php echo $feature['_feature_name']; ?></li>
              <?php } ?>
              </ul>
            </div>
          <?php } ?>
            <?php if ( $apartmentsArray[$apartmentAltID]['attributes_buildarea_m2'] !== null && $apartmentsArray[$apartmentAltID]['attributes_landarea_m2'] !== null ) { ?>
            <div class="col-48 mb-5">
              <h3> Apartment Areas </h3>
              <hr class="mb-4" />
              <ul class="list-unstyled lh-20">
                <li class="d-flex justify-content-between">
                  <span> Total Living Area </span>
                  <span> <?php echo intval($apartmentsArray[$apartmentAltID]['attributes_buildarea_m2']); ?> m² </span>
                </li>
                <li class="d-flex justify-content-between">
                  <span> Total Strata Area </span>
                  <span> <?php echo intval($apartmentsArray[$apartmentAltID]['attributes_landarea_m2']); ?> m² </span>
                </li>
              </ul>
            </div>
            <?php } ?>
            <?php if ( $apartmentsArray[$apartmentAltID]['development_pdf_floor_plans'] ||
              $apartmentsArray[$apartmentAltID]['development_pdf_plans'] ||
              $apartmentsArray[$apartmentAltID]['development_pdf_specifications'] ) { ?>
            <div class="col-48 mb-5">
              <h3> Downloads </h3>
              <hr class="mb-4" />
              <ul class="list-unstyled lh-20">
                <?php if ($apartmentsArray[$apartmentAltID]['development_pdf_floor_plans']) { ?>
                <li><a href="<?php echo $apartmentsArray[$apartmentAltID]['development_pdf_floor_plans']; ?>" class="btn btn-link btn-chevron-down-sm mb-2" role="button">
                  <span class="btn-chevron-text text-uppercase">Download Brochure</span>
                  <?php echo file_get_contents( get_stylesheet_directory_uri() . '/img/chevron-down.svg' ); ?>
                </a></li>
                <?php } ?>
                <?php if ($apartmentsArray[$apartmentAltID]['development_pdf_plans']) { ?>
                <li><a  href="<?php echo $apartmentsArray[$apartmentAltID]['development_pdf_plans']; ?>" class="btn btn-link btn-chevron-down-sm mb-2" role="button">
                  <span class="btn-chevron-text text-uppercase">Download Plans</span>
                  <?php echo file_get_contents( get_stylesheet_directory_uri() . '/img/chevron-down.svg' ); ?>
                </a></li>
                <?php } ?>
                <?php if ($apartmentsArray[$apartmentAltID]['development_pdf_specifications']) { ?>
                <li><a href="<?php echo $apartmentsArray[$apartmentAltID]['development_pdf_specifications']; ?>" class="btn btn-link btn-chevron-down-sm mb-0" role="button">
                  <span class="btn-chevron-text text-uppercase">Download Specifications</span>
                  <?php echo file_get_contents( get_stylesheet_directory_uri() . '/img/chevron-down.svg' ); ?>
                </a></li>
                <?php } ?>
              </ul>
            </div>
            <?php } ?>
          </div>
        </div>
        <div class="col-48 col-lg-4 order-2"></div>
        <div class="col-48 col-lg-22 mt-neg-7 mb-5 bg-dark sidebar sidebar-single-apartment d-flex flex-column order-1 order-md-3">
          <?php if ( $apartmentAltID && $apartmentsArray[$apartmentAltID]['development_slider_image'] ) { ?>
          <img src="<?php echo $apartmentsArray[$apartmentAltID]['development_slider_image']; ?>" class="img-fluid" />
          <?php } ?>
          <div class="p-5 d-block w-100">
          <?php if ( $apartmentAltID && $apartmentsArray[$apartmentAltID]['listing_agent_1_name'] && $apartmentsArray[$apartmentAltID]['listing_agent_1_phone_mobile'] ) { ?>
            <h4 class="text-primary mb-4"> 
              <?php echo $apartmentsArray[$apartmentAltID]['listing_agent_1_name']; ?> : <?php echo $apartmentsArray[$apartmentAltID]['listing_agent_1_phone_mobile']; ?>
            </h4>
          <?php } ?>
          <?php if ( $apartmentAltID && $apartmentsArray[$apartmentAltID]['address_street_number'] && $apartmentsArray[$apartmentAltID]['address_street_name'] && 
            $apartmentsArray[$apartmentAltID]['address_suburb_or_town'] && $apartmentsArray[$apartmentAltID]['address_state_or_region'] ) { ?>
            <h5 class="text-white fw-400 mb-3"> 
              Visit Our Display 
            </h5>
            <p class="text-white">
              <?php echo $apartmentsArray[$apartmentAltID]['address_street_number']; ?> <?php echo $apartmentsArray[$apartmentAltID]['address_street_name']; ?>, <br />
              <?php echo $apartmentsArray[$apartmentAltID]['address_suburb_or_town']; ?> <?php echo $apartmentsArray[$apartmentAltID]['address_state_or_region']; ?>, Australia
            </p>
          <?php } ?>
            <p class="text-white"> 
<?php
if ( $apartmentAltID && $apartmentsArray[$apartmentAltID]['development_opening_title'] && $apartmentsArray[$apartmentAltID]['development_open_hours'] ) {
  echo $apartmentsArray[$apartmentAltID]['development_opening_title'];
  foreach ( $apartmentsArray[$apartmentAltID]['development_open_hours'] as $hours_row ) {
    if ( $hours_row['text'] ) {
      echo $hours_row['text'] . " ";
    }
    if ( $hours_row['start_time'] ) {
      echo $hours_row['start_time'] . " ";
    }
    if ( $hours_row['end_time'] ) {
      echo " - " . $hours_row['end_time'];
    }
    echo '<br />';
  } 
}
?>
            </p>
            <?php if ( $apartmentAltID && $apartmentsArray[$apartmentAltID]['development_google_map']['image'] ) { ?>
            <img class="img-fluid d-block w-100 mb-3" 
              src="<?php echo $apartmentsArray[$apartmentAltID]['development_google_map']['image']; ?>  " />
            <?php } ?>
            <?php if ( $apartmentAltID && $apartmentsArray[$apartmentAltID]['development_google_map']['view_on_google_maps_link'] ) { ?>
            <p class="text-white mb-5"><small><a class="text-white" href="<?php echo $apartmentsArray[$apartmentAltID]['development_google_map']['view_on_google_maps_link']; ?>" target="_blank">
              Get Directions on Google Maps
            </a></small></p>
            <?php } ?>
            <a href="#footer-contact" class="btn btn-link btn-outline-white btn-hover-primary text-white text-uppercase p-4 fw-500" role="button"> 
              Request Viewing 
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="wrapper wrapper-apartment-features bg-light mb-5 mb-md-0">
    <div class="container-fluid">
      <div class="row">
        <div class="col-48 col-lg-22 gutters pr-0 pr-mb-2 py-5 mb-3">
          <div class="left-column-offset d-block">
          <?php if ( $apartmentAltID && $apartmentsArray[$apartmentAltID]['development_logo_black'] ) { ?>
            <img class="mb-5" style="width: 250px;" src="<?php echo $apartmentsArray[$apartmentAltID]['development_logo_black']; ?>" />
          <?php } ?>
            <h4 class="text-dark mb-"> Development Features </h4>
            <ul class="list-unstyled pr-5 mb-5">
              <li><?php echo $apartmentsArray[$apartmentAltID]['development_description']; ?></li>
            </ul>
            <a name="viewApartment" class="btn btn-link btn-arrow-right  btn-arrow-right-dark btn-arrow-right-hover-primary" target="_blank" href="<?php echo $apartmentsArray[$apartmentAltID]['development_website_link']['url']; ?>" role="button">
              <span class="btn-arrow-text"> Development Website </span>
              <?php echo file_get_contents($themePath . '/img/arrow-right-dark.svg'); ?> 
            </a>
          </div>
        </div>
        <div class="col-48 col-lg-26">
          <img src="<?php echo $apartmentsArray[$apartmentAltID]['development_our_developments_image']; ?>" class="img-fluid img-stretch" />
        </div>
      </div>
    </div>
    <script type="text/javascript">leftColumnOffset();</script>
  </div>

  <div class="wrapper wrapper-form-appointment">
    <div class="row row-footer-contact-area">
      <div class="container container-inner">
        <div class="row">
          <div class="col-48 footer-contact-col mx-auto gutters">
            <div class="footer-contact-form dp-1 bg-white px-6 py-6" id="footer-contact" data-aos="fade-up">
              <h2 class="text-dark mb-5"><small class="text-bold fw-700"> Request an <br /> Appointment </small></h2>
              <h5 class="mb-5 fw-300">
                <?php if ( get_field('phone', 'options') && get_field('phone_dial', 'options') ) { ?>
                <a href="tel:<?php the_field('phone_dial', 'options'); ?>" class="tel-link">
                  <span class="ls-0"> <?php the_field('phone', 'options'); ?> </span>
                </a>
                <?php } ?>
                &nbsp;&nbsp;
                <?php if ( get_field('email', 'options') ) { ?>
                <a href="mailto:sales@stirlingcapital.com.au" class="eml-link">sales@stirlingcapital.com.au</a>
                <?php } ?>
              </h5>
              <?php gravity_form( 3, false, false, false, '', true, 3 * 100 ); ?>
            </div>
            <div class="footer-contact-border dp-2"></div>
          </div>
        </div>
      </div> 
    </div>
  </div>

  <div class="wrapper">
    <div class="row">
      <div class="col-48">
        <div id="map"></div>
        <?php require( get_stylesheet_directory() . '/requires/require-gmap-single.php' ); ?>
      </div>
    </div>
  </div>

  <?php require( get_stylesheet_directory() . '/requires/require-featured-apartments.php' ); ?>

        </div>
      </div>
    </div>
    
  </div>
</div> 
<?php get_footer();
