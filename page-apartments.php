<?php
/**
 * Template Name: Apartments Search-Sort-Filter
 * @package mystirling
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly.
}

get_header();
$siteURL = get_bloginfo('url');
$themeURL  = get_stylesheet_directory_uri();
$siteTitle = get_bloginfo('Title');
$themePath = get_stylesheet_directory();
if ( get_field ( 'banner_image') ) {
  $banner_image = get_field ( 'banner_image');
} else {
  $banner_image['url'] = $siteURL . '/wp-content/uploads/apartments-bg.jpg';
}

?> 
<div class="wrapper" id="archive-wrapper">
  <div class="container-fluid archive-banner-apartments bg-dark" style="background-image:url('<?php echo $banner_image['url']; ?>')" >
    <div id="apartmentsRow" class="row pt-7">
       <div class="col-48 top-col">
        <div class="wrapper wrapper-apartments pt-7">
          <div id="app"></div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<?php get_footer();
