document.addEventListener( 'DOMContentLoaded', function( event ) {
  console.log( 'DOM fully loaded and parsed, loading footer JS' );
  if ( jQuery( '#fp-wrapper' ).length > 0 ) {
    console.log( 'firing slick' );
    var slickSlider = jQuery( '#fp-wrapper' )
      .slick( {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 4000,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        adaptiveHeight: false
      } );
    slickSlider.on( 'beforeChange', function( event, slick, currentSlide, nextSlide ) {
      jQuery( '.slide-nav' )
        .removeClass( 'active' );
      jQuery( '.slide-nav[data-slide-number="' + nextSlide + '"]' )
        .addClass( 'active' );
    } );
    if ( jQuery( '.slide-nav' ).length > 0 ) {
      jQuery( ".slide-nav" )
        .click( function( e ) {
          var slideNumber = jQuery( this )
            .attr( 'data-slide-number' );
          slickSlider.slick( 'slickGoTo', parseInt( slideNumber ) );
        } );
    }
  }

  if ( jQuery( '#developmentsSlider' ).length > 0 ) {
    jQuery( '#developmentsSlider' )
      .slick( {
        dots: true,
        arrows: true,
        infinite: true,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 2,
        appendDots: '#developmentsNavigation',
        appendArrows: '#developmentsNavigation',
        responsive: [
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              infinite: true,
              dots: true
            }
          }
        ]
      } );
  }

  if ( jQuery( '#insightsSlider' ).length > 0 ) {
    jQuery( '#insightsSlider' )
      .slick( {
        dots: true,
        arrows: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3,
        appendDots: '#insightsNavigation',
        appendArrows: '#insightsNavigation',
        responsive: [
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              infinite: true,
              dots: true
            }
          }
        ]
      } );
  }

  if ( jQuery( '#insightsSlider-archive' ).length > 0 ) {
    jQuery( '#insightsSlider-archive' )
      .slick( {
        dots: true,
        arrows: true,
        infinite: true,
        speed: 500,
        slidesPerRow: 3,
        rows: 2,
        slidesToScroll: 1,
        appendDots: '#insightsArchiveNavigation',
        appendArrows: '#insightsArchiveNavigation',
        responsive: [
          {
            breakpoint: 992,
            settings: {
              slidesPerRow: 2,
              rows: 2,
              slidesToScroll: 1,
            }
          },
          {
            breakpoint: 667,
            settings: {
              slidesPerRow: 1,
              rows: 3,
              slidesToScroll: 1
            }
          }
        ]
      } );
  }

  if ( jQuery( '#featuredApartments' ).length > 0 ) {
    jQuery( '#featuredApartments' )
      .slick( {
        dots: true,
        arrows: true,
        infinite: true,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 2,
        appendDots: '#featuredApartmentsNavigation',
        appendArrows: '#featuredApartmentsNavigation',
        responsive: [
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              infinite: true,
              dots: true
            }
          }
        ]
      } );
  }

  
  if ( jQuery( '#apartmentsSlider' ).length > 0 ) {
    jQuery( '#apartmentsSlider' )
      .slick( {
        dots: false,
        arrows: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        // appendArrows: '#apartmentsSliderNavigation'
      } );
  }

  if ( jQuery( '#insta-feed' ).length > 0 ) {
    var errorFunction = function( error ) {
      if ( error ) {
        console.log( 'Instagram feed error state' )
        jQuery( '.instagramContainer' )
          .hide();
      }
    }
    var feed = new Instafeed( {
      get: 'user',
      userId: 6765097937,
      clientId: '08c30cfbb12e4a279632c621dd280708',
      target: 'insta-feed',
      accessToken: '6765097937.08c30cf.96a0699273a548ffa6edc1e548ca03ef',
      resolution: 'standard_resolution',
      sortBy: 'most-recent',
      limit: 4,
      links: true,
      error: errorFunction,
      template: '<div class="col col-insta-feed mb-3 "> <a class="insta-feed-link dp-0" href="{{link}}" target="_blank" style="background-image: url(\'{{image}}\');"> </a> <i class="insta-feed-icon fa fa-instagram"></i></div>',
      after: function() {}
    } );
    feed.run();
  }

  AOS.init();

} );
