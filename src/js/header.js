function initSidebars() {

  if ( jQuery('#sidebar-right-enquire').length > 0 ) {
    var ldSidenavRightEnquire = jQuery( "#sidebar-right-enquire" ).ldSidenav({
      top: 0, 
      gap: 0, 
      zIndex: 1401, 
      align: "right",
      attr: "nav-open",
      selectors: {
        trigger: "#sidebar-right-enquire-button"
      },
      sidebar: {
        width: 1088
      },
      events: {
        callbacks: {
          animation: {
            freezePage: true
          }
        }
      }
    });
  }

  if ( jQuery('#sidebar-left-nav').length > 0 ) {
    var ldSidenavLeft = jQuery( "#sidebar-left-nav" ).ldSidenav({
      top: 0, 
      gap: 0, 
      zIndex: 1502, 
      align: "left",
      attr: "nav-open",
      selectors: {
        trigger: "#sidebar-left-nav-button"
      },
      sidebar: {
        width: 992
      },
      events: {
        callbacks: {
          animation: {
            freezePage: true
          }
        }
      }
    });
  }

}

function leftPadOffset() {
  var navbarBrand = jQuery('#myStirlingNavbarBrand');
  var windowWidth = jQuery( window ).width();
  var containerWidth = jQuery( '#wrapperContainerReference > .container' ).width();
  var offsetAmount = ( windowWidth - containerWidth ) / 2;
  var leftOffset = offsetAmount - 92; 
  if ( windowWidth > 1367 ) {
    navbarBrand.animate( {left:leftOffset}, {duration:100, easing:"easeOutStrong"});
  }
  if ( windowWidth <= 1367 ) {
    navbarBrand.animate( {left:2}, {duration:100, easing:"easeOutStrong"});
  }
  console.log('leftOffset is currently: '+leftOffset);
}

function leftColumnOffset() {
  if (jQuery('.left-column-offset').length > 0 ) {
    var leftColumn = jQuery('.left-column-offset');
    var leftColumnwindowWidth = jQuery( window ).width();
    var leftColumncontainerWidth = jQuery( '#wrapperContainerReference > .container' ).width();
    var leftColumnleftOffset = ( leftColumnwindowWidth - leftColumncontainerWidth ) / 2;
    if ( leftColumnwindowWidth > 619 ) {
      leftColumn.animate( {paddingLeft:leftColumnleftOffset}, {duration:100, easing:"easeOutStrong"});
    }
    if ( leftColumnwindowWidth <= 619 ) {
      leftColumn.animate( {paddingLeft:0}, {duration:100, easing:"easeOutStrong"});
    }
    console.log('leftOffset is currently: '+leftColumnleftOffset);
  }
}

jQuery( window ).resize( _.throttle( function() {
  leftPadOffset();
  leftColumnOffset();
}, 125 ) );
