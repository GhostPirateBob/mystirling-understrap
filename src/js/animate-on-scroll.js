AOS.init({
  disable: '',
  startEvent: 'DOMContentLoaded',
  initClassName: 'aos-init',
  animatedClassName: 'aos-animate',
  useClassNames: false,
  disableMutationObserver: false,
  debounceDelay: 100,
  throttleDelay: 20,
  offset: 80,
  duration: 250,
  easing: 'ease-out',
  once: true,
  mirror: false,
  anchorPlacement: 'top'
});