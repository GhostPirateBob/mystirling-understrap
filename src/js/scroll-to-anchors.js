jQuery( 'a[href*="#"]' )
.not( '[href="#"]' )
.not( '[href="#0"]' )
.click( function( event ) {
  if ( location.pathname.replace( /^\//, '' ) == this.pathname.replace( /^\//, '' ) && location.hostname == this
    .hostname ) {
    var target = jQuery( this.hash );
    target = target.length ? target : jQuery( '[name=' + this.hash.slice( 1 ) + ']' );

    if ( target.length ) {
      event.preventDefault();
      target.get( 0 )
        .scrollIntoView({
        offsetTop: 150,
        behavior: "smooth", 
        block: "start", 
        inline: "nearest"
        })
        .delay( 1024 )
        .focus();
        if ( target.is( ":focus" ) ) {
          return false;
        }
        else {
          target.attr( 'tabindex', '-1' );
          target.focus();
        };
    }
  }
} );