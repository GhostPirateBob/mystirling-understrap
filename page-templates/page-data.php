<?php
/**
 * Template Name: Data Debug
 * @package mystirling
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly.
}

get_header();
?>

<div class="wrapper" id="page-wrapper">
  <div class="container-fluid" id="content" tabindex="-1">
    <div class="row row-entry-content">
      <div class="col-md-48 content-area entry-content" id="primary">


<div class="row pt-7 pb-7">
  <div class="col-48 gutters">
    <?php require( get_stylesheet_directory() . '/requires/require-api-data.php' ); ?>
  </div>
</div>
<?php
while ( have_posts() ) {
  the_post();
  the_content();
} // end of the loop. 
?>
      </div>
    </div>
    
  </div>
</div> 
<?php get_footer();
