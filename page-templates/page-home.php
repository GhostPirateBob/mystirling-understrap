<?php
/**
 * Template Name: Home Page Template
 *
 * @package mystirling
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly.
}

get_header();
$siteURL   = get_bloginfo('url');
$themeURL  = get_stylesheet_directory_uri();
$siteTitle = get_bloginfo('Title');
$themePath = get_stylesheet_directory();
$container = get_theme_mod('understrap_container_type');
$treehouse_image = get_field( 'treehouse_image' );
$cirque_image = get_field( 'cirque_image' );
$barque_image = get_field( 'barque_image' );
$twentysix_image = get_field( '26_image' );
$verdant_image = get_field( 'verdant_image' );
$overlap_image = get_field( 'overlap_image' );
$team_image = get_field( 'team_image' );
$promotional_block_heading = get_field( 'promotional_block_heading');

?> 
<div class="wrapper" id="page-wrapper">
  <div class="container-fluid" id="content" tabindex="-1">
    <div class="row row-entry-content">
      <div class="col-md-48 content-area entry-content" id="primary">

        <?php require( get_stylesheet_directory() . '/requires/require-slider.php' ); ?>

        <div id="apartmentsRow" class="row">
          <div class="col-48">
            <div class="wrapper wrapper-apartments">
              <div id="app"></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-48">
            <div id="map"></div>
          </div>
        </div>

        <?php require( get_stylesheet_directory() . '/requires/require-gmap.php' ); ?>

        <?php require( get_stylesheet_directory() . '/requires/require-promotions.php' ); ?>

        <?php require( get_stylesheet_directory() . '/requires/require-featured-apartments.php' ); ?>

        <?php require( get_stylesheet_directory() . '/requires/require-developments.php' ); ?>

        <?php require( get_stylesheet_directory() . '/requires/require-instafeed.php' ); ?>

        <?php require( get_stylesheet_directory() . '/requires/require-places-lifestyle.php' ); ?>

        <?php require( get_stylesheet_directory() . '/requires/require-insights.php' ); ?>
        
        <?php require( get_stylesheet_directory() . '/requires/require-about.php' ); ?>

      </div>
    </div>
  </div>
</div> 
<?php get_footer();
