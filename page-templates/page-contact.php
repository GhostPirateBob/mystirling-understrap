<?php
/**
 * Template Name: Contact Page
 * @package mystirling
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly.
}

$siteURL   = get_bloginfo('url');
$themeURL  = get_stylesheet_directory_uri();
$siteTitle = get_bloginfo('Title');
$themePath = get_stylesheet_directory();
$container = get_theme_mod('understrap_container_type');

if ( get_field('right_enquire', 'option') ) {
  $nav_right_form = get_field('right_enquire', 'option');
  $nav_right_form_title = $nav_right_form['title'];
  $nav_right_form_id = RGFormsModel::get_form_id( $nav_right_form_title ); 
}

get_header();

?>

<div class="wrapper" id="page-wrapper">
  <div class="container-fluid" id="content" tabindex="-1">
    <div class="row row-entry-content">
      <div class="col-md-48 content-area entry-content" id="primary">
        <div class="wrapper pt-4 pt-lg-7 pb-4 pb-lg-7 bg-light">
          <div class="container">
            <div class="row">
              <div class="col-48 gutters">
                <div class="d-block w-100"> 
                  <h2 class="text-dark mb-3 mb-lg-5 lh-1"><small class="text-bold fw-700"> Visit us at our displays or <br /> drop us a line. </small></h2>
                  <h5 class="mb-5"><span class="ls-0 pr-3 d-block d-lg-inline">+61 (8) 9382 1616</span><a href="mailto:sales@stirlingcapital.com.au">sales@stirlingcapital.com.au</a></h5>
                </div>
              </div>
              <div class="col-48 col-md-12 col-lg-16 col-xl-20 gutters"></div>
            </div> 
<?php if ( get_field('right_enquire', 'option') ) { ?> 
            <div class="container container-inner">
              <div id="contactPageForm" class="row">
                <div class="col-48">
                  <?php gravity_form( $nav_right_form_id, false, false, false, '', true, $nav_right_form_id * 100 ); ?>
                </div>
              </div> 
            </div>
<?php } ?>
          </div>
        </div>
        <div class="container py-3 py-lg-5">
<?php
$query = new WP_Query(array(
    'post_type' => 'developments',
    'post_status' => 'publish',
    'orderby' => 'menu_order',
    'post__not_in' => array(672),
    'order' => 'none',
    'posts_per_page' => -1
));
while ($query->have_posts()) {
    $query->the_post();
    require( get_stylesheet_directory() . '/loop-templates/loop-contact.php' );
}
wp_reset_query();
?>
        </div>
      </div>
    </div>
  </div>
</div> 





<?php get_footer();
