<?php
/**
 * Template Name: About Page Template
 *
 * @package mystirling
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly.
}

get_header();

$siteURL = get_bloginfo('url');
$themeURL  = get_stylesheet_directory_uri();
$siteTitle = get_bloginfo('Title');
$themePath = get_stylesheet_directory();
$team_content = get_field( 'team_content');
$team_image = get_field( 'team_image');
$about_content = get_field( 'about_content');
$highlight_heading = get_field( 'highlight_heading');
?> <div class="wrapper" id="page-wrapper">
  <div class="container-fluid" id="content" tabindex="-1">
    <div class="row row-entry-content">
      <div class="col-md-48 content-area entry-content" id="primary">
        <div class="container-fluid mb-5 pb-0 pb-lg-7">
          <div class="subpage-banner row">
            <img src="<?php echo get_bloginfo('url'); ?>/wp-content/uploads/hard-hats.jpg" class="img-fit">
            <div class="subpage-banner-inner row w-100 d-flex align-self-end">
              <div class="container ">
                <div class="row">
                  <div class="col-auto mr-auto bg-primary overlap-title" data-aos="fade-up">
                    <h2 class="page-title text-secondary mw-28 mb-0"> <?php echo get_the_title();?> <br> myStirling </h2>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="container container-inner aboutInfo">
              <div class="row d-flex align-items-center">
                <div class="col-48 col-lg-25 gutters pr-lg-4" data-aos="fade-up">
                  <h5 class="font-weight-normal ls-0 lh-13 mb-4"> <?php echo $team_content;?> </h5>
                  <a class="btn btn-link  mt-auto mb-5" type="link" href="//www.stirlingcapital.com.au/"
                    target="_blank">
                    <span class="btn-arrow-text text-uppercase">STIRLING CORPORATE WEBSITE
                      <?php echo file_get_contents($themePath . '/img/arrow-right-dark.svg'); ?> </span>
                  </a>
                  <img class="img-fit mb-5" src="<?php echo $team_image['url'];?>" />
                </div>
                <div class="col-48 col-lg-23 gutters pl-lg-5">
                  <div class="row mb-lg-5">
                    <h3 class="text-primary callout_heading mb-3 mb-lg-5" data-aos="fade-up">
                      <?php echo $highlight_heading;?>
                    </h3>
                  </div>
                  <div class="row">
                    <div class="team_content mb-5 mb-lg-0" data-aos="fade-up"> <?php echo $about_content;?> </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> 
<?php get_footer();
