<?php if ( get_row_layout() == 'slider-group' ) : ?>
			<?php if ( have_rows( 'slider_group' ) ) : ?>
				<?php while ( have_rows( 'slider_group' ) ) : the_row(); ?>
					<?php $background_image = get_sub_field( 'background_image' ); ?>
					<?php if ( $background_image ) { ?>
						<img src="<?php echo $background_image['url']; ?>" alt="<?php echo $background_image['alt']; ?>" />
					<?php } ?>
				<?php endwhile; ?>
			<?php endif; ?>
		<?php endif; ?>
