<?php
/**
 * Template Name: Developments Archive Page
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$siteURL = get_bloginfo('url');
$themeURL  = get_stylesheet_directory_uri();
$siteTitle = get_bloginfo('Title');
$themePath = get_stylesheet_directory();
$archivePageID = get_the_ID();
$banner_image = get_field ( 'banner_image', $archivePageID);
$banner_content = get_field ('banner_content' , $archivePageID);
?> 
<div class="wrapper" id="archive-wrapper">
  <div class="container-fluid">
    <div class="subpage-banner row">
      <img src="<?php echo $banner_image['url'];?>" class="img-fit">
      <div class="subpage-banner-inner row w-100 d-flex align-self-end">
        <div class="container">
          <div class="row">
            <div class="col-auto z-11" data-aos="fade-up">
              <h2 class="p-h2-block bg-primary text-secondary dp-00 mb-0"> Our <br /> Developments </h2>
            </div>
            <div class="container container-inner">
              <div class="row">
                <div class="col-48 col-lg-32 dp-0 z-10" data-aos="fade-left">
                  <div class="banner-content bg-white"><?php echo $banner_content;?></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
    <div class="row ">
      <div class="col-48">
        <div id="map"></div>
        <?php require( get_stylesheet_directory() . '/requires/require-gmap.php' ); ?>
      </div>
    </div>
    </div>
    <div class="container" id="content" tabindex="-1">
      <div class="row">
        <div class="col content-area" id="primary">
        <div class="entry-content"> 
          <div class="row row-apartments-filters py-5 d-none d-lg-block"></div>
<?php
$query = new WP_Query(array(
  'post_type' => 'developments',
  'post_status' => 'publish',
  'orderby' => 'menu_order',
  'order' => 'ASC',
  'posts_per_page' => -1
));
while ($query->have_posts()) {
  $query->the_post();
  get_template_part( 'loop-templates/loop-developments' );
}
wp_reset_query();
?> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div><?php // need this extra closing tag ?>
</div>
<?php get_footer();
