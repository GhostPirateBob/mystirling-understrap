<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}
$container    = get_theme_mod('understrap_container_type');
$siteURL      = get_site_url();
$themeURL     = get_stylesheet_directory_uri();
$themePath    = get_stylesheet_directory();
$siteTitle    = get_bloginfo('title');

if ( get_field('logo', 'option') ) {
  $logo = get_field('logo', 'option');
}

if ( get_field('left_enquire', 'option') ) {
  $nav_left_form = get_field('left_enquire', 'option');
  $nav_left_form_title = $nav_left_form['title'];
  $nav_left_form_id = RGFormsModel::get_form_id( $nav_left_form_title ); 
}

if ( get_field('right_enquire', 'option') ) {
  $nav_right_form = get_field('right_enquire', 'option');
  $nav_right_form_title = $nav_right_form['title'];
  $nav_right_form_id = RGFormsModel::get_form_id( $nav_right_form_title ); 
}

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="icon" type="image/png" href="<?php echo get_bloginfo('url'); ?>/favicon.ico">
  <link
    href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i,900,900i&display=swap"
    rel="stylesheet">
<?php if ( is_page('apartments') || is_front_page() ) { ?>
  <link rel=stylesheet href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link href="<?php echo get_stylesheet_directory_uri() . '/app/dist/app-chunk.js'; ?>" rel="preload" as="script">
  <link href="<?php echo get_stylesheet_directory_uri() . '/app/dist/app.css'; ?>" rel="preload" as="style">
  <link href="<?php echo get_stylesheet_directory_uri() . '/app/dist/app.js'; ?>" rel="preload" as="script">
  <link href="<?php echo get_stylesheet_directory_uri() . '/app/dist/app.css'; ?>" rel="stylesheet">
<?php } ?>
  <script src="<?php echo $themeURL; ?>/js/jquery-3.4.1.migrate/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/easing/EasePack.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/plugins/CSSPlugin.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/jquery.gsap.min.js"></script>
  <script type="text/javascript">
<?php if ( file_exists( $themePath . '/src/js/browser-detection.min.js' ) ) {
  echo file_get_contents( $themePath . '/src/js/browser-detection.min.js' );
} ?>
<?php if ( file_exists( $themePath . '/src/js/feature-detection.min.js' ) ) {
  echo file_get_contents( $themePath . '/src/js/feature-detection.min.js' );
} ?>
  </script>
  <?php wp_head(); ?>
  <style type="text/css">
  </style>
</head>
<body <?php body_class(); ?>>
  <div class="site" id="page">
    <div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">
      <div class="row">
        <a class="skip-link sr-only sr-only-focusable"
          href="#content"><?php esc_html_e( 'Skip to content', 'understrap' ); ?></a>
        <div id="navCol" class="col-48 w-100 d-flex justify-content-between bg-white z-1504 dp-00">
          <button id="sidebar-left-nav-button" type="button"
            class="btn tcon tcon-menu--xcross sidebar-left-toggler bs-0" aria-label="toggle menu">
            <span class="tcon-menu__lines" aria-hidden="true"></span>
            <span class="tcon-visuallyhidden"> toggle menu </span>
          </button>
          <nav id="navMain" class="navbar navbar-expand-md navbar-light w-100">
            <?php if ( get_field('logo', 'option') ) { ?> <a id="myStirlingNavbarBrand" class="navbar-brand"
              href="<?php echo get_home_url(); ?>">
              <?php loadedImage($logo, ['d-inline-block', 'align-top', 'img-fluid']); ?> </a> <?php } ?> 
              <div class="row">
              <div class="col d-flex align-items-center justify-content-start ">
                <a href="<?php echo $siteURL; ?>" class="d-none w-100 mr-auto" alt="<?php echo $siteTitle; ?>">
                  <?php if ( get_field('logo_mobile', 'option') ) { ?> <img
                    class="img-fluid logo-mobile d-none"
                    src="<?php echo get_field('logo_mobile', 'option'); ?>" /> <?php } ?> </a>
              </div>
              <div class="col-auto d-none d-lg-block">
                <button id="btn-live-chat" type="button" class="btn btn-link btn-light btn-icon-left btn-header-link text-black"
                  aria-label="enquire now">
                  <?php if ( file_exists( $themePath . '/img/icon-live-chat.svg' ) ) {
                    echo file_get_contents( $themePath . '/img/icon-live-chat.svg' );
                  } ?>
                  Let's Chat 
                </button>
              </div>
              <div class="col-auto d-none d-lg-block">
                <a id="btn-find-apartment" href="<?php echo bloginfo('url'); ?>/apartments" role="button"
                  class="btn btn-link btn-light btn-header-link text-black"> Find my Apartment </a>
              </div>
              <div class="col-auto d-none d-lg-block">
                <a id="btn-our-developments" href="<?php echo bloginfo('url'); ?>/developments" role="button"
                  class="btn btn-link btn-light btn-header-link text-black"> Our Developments </a>
              </div>
              <div class="col-auto ">
                <button id="sidebar-right-enquire-button" type="button"
                  class="btn btn-enquire btn-header-link text-uppercase text-white"> Enquire<span class="d-none d-lg-block">&nbsp;Now</span> </button>
              </div>
          </nav>
        </div>
        <div class="ld-sidebar sidebar left sidebar-left sidebar-left-nav bg-white" id="sidebar-left-nav">
          <div class="wrapper pb-lg-5">
            <div class="row w-100 d-none d-lg-block">
              <div class="col-48 gutters">
                <h3 class="mt-3 mb-5 text-black mw-lg-20 fw-400"> Creating places where people want to live. </h3>
              </div>
            </div>
            <div class="row d-block d-sm-none">
              <div class="col-auto ">
                <a id="btn-find-apartment" href="<?php echo bloginfo('url'); ?>/apartments" role="button"
                  class="btn btn-light text-black w-100 text-capitalize py-3 gutters"> Find my Apartment </a>
              </div>
              <div class="col-auto ">
                <a id="btn-our-developments" href="<?php echo bloginfo('url'); ?>/developments" role="button"
                  class="btn btn-light text-black text-capitalize w-100 py-3 mt-1 gutters"> Our Developments </a>
              </div>
            </div>
            <div class="row">
              <div class="col-48 col-sm-24 gutters">
                <div class="row">
                  <div class="col-48 mb-2 mb-md-5 mt-3"> 
                  <h5 class="font-weight-normal d-block d-sm-none text-primary text-uppercase lh-15 ls-25 mt-0">MyStirling</h5>
<?php wp_nav_menu(
  array(
    'menu'            => 'header_nav',
    'theme_location'  => 'header_nav',
    'menu_id'         => 'sidebar-mystirling-nav',
    'fallback_cb'     => false,
    'container_class' => '',
    'container'       => '',
    'container_id'    => '',
    'menu_class'      => 'navbar-nav list-unstyled',
    'depth'           => 1,
    'walker'          => new NavWalker(),
    'echo'            => true,
    'before'          => '',
    'after'           => '',
    'link_before'     => '',
    'link_after'      => ''
  )
); ?> 
                  </div> 
                  <div class="col-48 mb-3 mb-lg-5 d-none d-sm-block"> <?php if ( get_field('instagram_url', 'option') ) { ?> <a
                      href="<?php echo strip_tags(get_field('instagram_url', 'option')); ?>"
                      class="footer-multisite-social-link mr-3" target="_blank" rel="nofollow">
                      <i class="fa fa-instagram text-dark"></i>
                    </a> <?php } ?> <?php if ( get_field('facebook_url', 'option') ) { ?> <a
                      href="<?php echo strip_tags(get_field('facebook_url', 'option')); ?>"
                      class="footer-multisite-social-link" target="_blank" rel="nofollow">
                      <i class="fa fa-facebook text-dark"></i>
                    </a> <?php } ?> </div>
                  <div class="col-48 mb-3 d-none d-sm-block">
                    <button id="btn-live-chat" type="button"
                      class="btn btn-link btn-icon-left btn-header-link text-primary text-uppercase mb-0 p-0 b-0"
                      aria-label="enquire now">
                      <svg id="icon-live-chat" class="img-fluid icon icon-live-chat d-block mr-auto"
                        xmlns="http://www.w3.org/2000/svg" width="18px" height="18px" version="1">
                        <path fill="#24c7c4"
                          d="M9 16.3c.8-.7 1.5-1.4 2.2-2.1.3-.3.6-.4 1-.4h3.5c.7 0 .9-.2.9-.9V2.2c0-.6-.2-.9-.9-.9H2.2c-.6 0-.9.2-.9.9v10.7c0 .6.2.9.9.9H6c.3 0 .6.1.8.3.7.8 1.5 1.5 2.2 2.2M16.4 0c.9 0 1.6.7 1.6 1.6v11.5c0 1.3-.9 2.2-2.1 2.2h-3.4c-.2 0-.4.1-.6.2-.8.7-1.5 1.5-2.3 2.2-.5.4-.8.4-1.2 0-.7-.7-1.4-1.3-2-2.1-.3-.3-.6-.4-1-.4H2.6c-1.3.1-2.2-.4-2.6-1.7V1.6C0 .7.8 0 1.6 0h14.8z" />
                        <path fill="#24c7c4"
                          d="M9 5.5h4c.3 0 .6.1.8.4.2.5-.1 1-.7 1H5c-.5 0-.8-.3-.8-.7 0-.4.3-.7.8-.7h4zM7.6 8.3h2.6c.5 0 .8.3.8.7 0 .4-.3.7-.8.7H4.9c-.4 0-.8-.3-.7-.7 0-.4.3-.7.8-.7h2.6" />
                      </svg> <br /> &nbsp;Let's Chat </button>
                  </div>
                </div>
              </div>
              <div class="col-48 col-sm-24 gutters"> 
                <div class="row">
                <div class="col-48 mb-2 mb-md-5 mt-3"> 
                  <h5 class="font-weight-normal d-block d-sm-none text-primary text-uppercase lh-15 ls-25 mt-0">Development Websites</h5>
<?php wp_nav_menu(
  array(
    'menu'            => 'subsite_nav',
    'theme_location'  => 'subsite_nav',
    'menu_id'         => 'sidebar-mystirling-subsites',
    'fallback_cb'     => false,
    'container_class' => '',
    'container'       => '',
    'container_id'    => '',
    'menu_class'      => 'navbar-nav list-unstyled',
    'depth'           => 1,
    'walker'          => new NavWalker(),
    'echo'            => true,
    'before'          => '',
    'after'           => '',
    'link_before'     => '',
    'link_after'      => ''
  )
); ?>
                </div>
                <div class="header-img-wrapper d-block mb-3" >
                  <?php echo file_get_contents( $themePath . '/img/logo-stirling-capital.svg'); ?>
                  <a type="link" href="//www.stirlingcapital.com.au/"
                    target="_blank">
                  <p class="mt-lg-2 mt-0 mb-0 mb-lg-2 footer-wrapper-text d-block"><small> Visit the Stirling <br class="d-none d-sm-block"> Corporate website </small></p>
                  <?php echo file_get_contents($themePath . '/img/arrow-right-dark.svg'); ?></a>
                </div>
                <?php /*
                <div class="mobile-social w-100 d-flex flex-row align-items-center d-flex d-sm-none">
                  <div class="col-24 mb-0 mb-lg-5"> <?php if ( get_field('instagram_url', 'option') ) { ?> <a
                        href="<?php echo strip_tags(get_field('instagram_url', 'option')); ?>"
                        class="footer-multisite-social-link mr-3" target="_blank" rel="nofollow">
                        <i class="fa fa-instagram text-dark"></i>
                      </a> <?php } ?> <?php if ( get_field('facebook_url', 'option') ) { ?> <a
                        href="<?php echo strip_tags(get_field('facebook_url', 'option')); ?>"
                        class="footer-multisite-social-link" target="_blank" rel="nofollow">
                        <i class="fa fa-facebook text-dark"></i>
                      </a> <?php } ?> </div>
                    <div class="col-24 mb-3">
                      <button id="btn-live-chat" type="button"
                        class="btn btn-link btn-icon-left btn-header-link text-primary text-uppercase mb-0 p-0 b-0"
                        aria-label="enquire now">
                        <svg id="icon-live-chat" class="img-fluid icon icon-live-chat d-block mr-auto"
                          xmlns="http://www.w3.org/2000/svg" width="18px" height="18px" version="1">
                          <path fill="#24c7c4"
                            d="M9 16.3c.8-.7 1.5-1.4 2.2-2.1.3-.3.6-.4 1-.4h3.5c.7 0 .9-.2.9-.9V2.2c0-.6-.2-.9-.9-.9H2.2c-.6 0-.9.2-.9.9v10.7c0 .6.2.9.9.9H6c.3 0 .6.1.8.3.7.8 1.5 1.5 2.2 2.2M16.4 0c.9 0 1.6.7 1.6 1.6v11.5c0 1.3-.9 2.2-2.1 2.2h-3.4c-.2 0-.4.1-.6.2-.8.7-1.5 1.5-2.3 2.2-.5.4-.8.4-1.2 0-.7-.7-1.4-1.3-2-2.1-.3-.3-.6-.4-1-.4H2.6c-1.3.1-2.2-.4-2.6-1.7V1.6C0 .7.8 0 1.6 0h14.8z" />
                          <path fill="#24c7c4"
                            d="M9 5.5h4c.3 0 .6.1.8.4.2.5-.1 1-.7 1H5c-.5 0-.8-.3-.8-.7 0-.4.3-.7.8-.7h4zM7.6 8.3h2.6c.5 0 .8.3.8.7 0 .4-.3.7-.8.7H4.9c-.4 0-.8-.3-.7-.7 0-.4.3-.7.8-.7h2.6" />
                        </svg> <br /> &nbsp;Let's Chat </button>
                    </div>
                  </div> 
                  */ ?>
              </div>
            </div>
          </div>
        </div>
        <div class="ld-sidebar sidebar right sidebar-right sidebar-right-enquire" id="sidebar-right-enquire">
          <div class="wrapper pt-7 pb-5 px-5 px-lg-6">
            <div class="row">
              <div class="col-48 col-md-36 col-lg-32 col-xl-28 gutters pt-6">
                <div class="d-block w-100"> 
                  <?php if ( get_field( 'right_enquire_heading', 'option' ) ) { ?> 
                    <h3 class="mt-3 mb-5 text-white mw-lg-30 fw-400"> <?php the_field('right_enquire_heading', 'option'); ?> </h3> 
                  <?php } ?> 
                </div>
              </div>
              <div class="col-48 col-md-12 col-lg-16 col-xl-20 gutters"></div>
            </div> 
            <?php if ( get_field('right_enquire', 'option') ) { ?> 
            <div class="row">
              <div class="col-48">
                <?php gravity_form( $nav_right_form_id, false, false, false, '', true, $nav_right_form_id * 100 ); ?>
              </div>
            </div> 
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
    <div id="wrapperContainerReference" class="wrapper wrapper-container-reference d-block w-100" 
      style="height: 0px !important; visibility: hidden !important; background: none !important;">
      <div class="container container-inner gutters"></div>
    </div>
  <script type="text/javascript">
    leftPadOffset();
    initSidebars();
  </script>
  </div>
