<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
?>

<div class="wrapper" id="single-wrapper">
  <div class="container-fluid" id="content" tabindex="-1">
    <div class="row">
      <div class="col content-area" id="primary">
        <div class="entry-content">
        <?php
while ( have_posts() ) {
  the_post();
  the_content();

  // loop over the ACF flexible fields for this page / post 
  while ( the_flexible_field('content_flexi') ) {
    // load the flexi field from the components folder
    get_template_part( 'components/'. get_row_layout() );
  }
} // end of the loop. 
?>
        </div>
      </div>
    </div> 
  </div>
</div><?php // need this extra closing tag ?>
</div>
<?php get_footer();
