<?php
/**
 * @package understrap
 * Template Name: Insights Archive Page
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly.
}

get_header();
$siteURL         = get_bloginfo('url');
$themeURL        = get_stylesheet_directory_uri();
$siteTitle       = get_bloginfo('Title');
$themePath       = get_stylesheet_directory();
$container       = get_theme_mod('understrap_container_type');
$archivePageID = get_the_ID();
?> 
<div class="wrapper" id="archive-wrapper">
  <div class="container-fluid bg-light">
    <div class="subpage-banner row pb-3 pb-lg-5">
      <img src="<?php echo $siteURL . "/wp-content/uploads/Barque.jpg"; ?>" class="img-fit ">
      <div class="row w-100 d-flex align-self-end mt-neg-5">
        <div class="container">
          <div class="row mb-0 mb-lg-5">
            <div class="col-auto bg-primary z-10 dp-0 mb-2 mb-lg-7" data-aos="fade-up">
              <h2 class="page-title text-secondary mb-0 mt-0"> <?php the_title(); ?> </h2>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container" id="content" tabindex="-1">
      <div class="row">
        <div class="col content-area" id="primary">    
        <div class="entry-content">
  <?php $query = new WP_Query(array(
    'posts_per_page' => 1,
    'post_type'      => 'insights',
    'meta_key'       => 'featured_insight',
    'meta_compare'   => '=',
    'meta_value'     => 1,
  ));
  while ( $query->have_posts() ) {
    $query->the_post();
    $insights_post_image = get_field('insights_post_image');
  ?> 
              
              <div class="row pb-5 mb-5 pt-0 pt-lg-7 border-bottom border-dark" data-aos="fade-down"> 
                <div class="col-48 col-lg-29">
                  <div class="post-img gutters pb-3 pb-md-0">
                    <a href="<?php the_permalink(); ?>">
                      <img class="img-fit" src="<?php echo $insights_post_image['url']; ?>">
                    </a>
                  </div>
                </div>
                <div class="col-48 col-lg-19 gutters align-self-end px-lg-5 " data-aos="fade-up">
                  <h3 class="mw-lg-27 font-weight-bold mb-4">
                    <a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a>
                  </h3>
                  <a class="btn btn-link btn-arrow-right btn-arrow-right-dark btn-arrow-right-hover-primary mt-auto"
                    type="link" href="<?php the_permalink(); ?>">
                    <span class="btn-arrow-text text-uppercase">Read More
                    <?php echo file_get_contents($themePath . '/img/arrow-right-dark.svg'); ?> </span>
                   
                  </a>
                </div> 
              </div>
  <?php } wp_reset_query(); ?>
            <div id="insightsSlider-archive" class="row card-deck w-100 mb-0">
  <?php
$query = new WP_Query(array(
  'post_type' => 'insights',
  'post_status' => 'publish',
  'orderby' => 'menu_order',
  'order' => 'ASC',
  'posts_per_page' => -1
));
while ($query->have_posts()) {
  $query->the_post();
  $insights_post_image = get_field('insights_post_image'); 
?> 
              <div class="col-48 mb-5" data-aos="fade-up">
                <div class="card gutters">
                  <a href="<?php the_permalink(); ?>">
                  <?php if ($insights_post_image) {?>
                    <img class="img-fit" src="<?php echo $insights_post_image['url']; ?>">
                  <?php } else { ?>
                    <img class="img-fit" src="/wp-content/uploads/08092016-Kishorn-Article.png"/>
                    <?php } ?>
                    <div class="card-body">
                      <h3 class="insights-title mb-4"><?php the_title(); ?></h3>
                      <span class="btn btn-link btn-arrow-right btn-arrow-right-dark btn-arrow-right-hover-primary" href="<?php the_permalink(); ?>" role="button">
                      <span class="btn-arrow-text">  Read More </span>
                        <?php echo file_get_contents($themePath . '/img/arrow-right-dark.svg'); ?> 
                      </span>
                    </div>
                  </a>
                </div>
              </div> 
<?php
}
wp_reset_query();
?> 
            </div> 
            <div id="insightsArchiveNavigation" class="slick-navigation w-100 d-flex justify-content-center"></div>
          </div>
        </div> 
      </div>
    </div>
  </div>
</div><?php // need this extra closing tag ?>
<?php get_footer(); ?>
