<?php
/**
 * Understrap functions and definitions
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}

update_option('uploads_use_yearmonth_folders', 0);

add_theme_support('soil-clean-up');

// add_theme_support('soil-disable-asset-versioning');
add_theme_support('soil-disable-trackbacks');

// add_theme_support('soil-jquery-cdn');
// add_theme_support('soil-js-to-footer');

add_theme_support('soil-nice-search');
add_theme_support('soil-relative-urls');

add_image_size( 'gmap-img-top', 754, 452, true );
add_image_size( 'gmap-img-static', 754, 452, true );

function acf_google_api_key() {
  
  acf_update_setting('google_api_key', 'AIzaSyBw-Ld2uyp74D05cs6QOK3CVpnKcjxJ1e0');
}

add_action('acf/init', 'acf_google_api_key');

$understrap_includes = array(
  // '/custom-post-export.php',          // [Custom] Load Custom Post Types
  // '/custom-field-export.php',          // [Custom] Load Custom ACF Fields
  // '/sticky.php',                       // [Custom] Enable Stick Posts
  // '/widgets.php',                      // Register widget area.
  '/enqueue.php',                         // Enqueue scripts and styles.
  '/custom.php',                          // Load custom functions.
  '/gform-bootstrap.php',                 // [Custom] GForm Bootstrap 4 Forms
  '/theme-settings.php',                  // Initialize theme default settings.
  '/setup.php',                           // Theme setup and custom theme supports.
  '/template-tags.php',                   // Custom template tags for this theme.
  '/pagination.php',                      // Custom pagination for this theme.
  '/hooks.php',                           // Custom hooks.
  '/extras.php',                          // Custom functions that act independently of the theme templates.
  '/customizer.php',                      // Customizer additions.
  '/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker.
  '/class-soil-navwalker.php',            // Soil nav walker.
  '/editor.php',                          // Load Editor functions.
  '/deprecated.php',                      // Load deprecated functions.
  '/gmap-static-img.php'                  // [Custom] Google Maps Static Img
);

add_filter( 'gform_field_value_development', 'populate_development' );
function populate_development( $value ) {
   return '';
}

foreach ( $understrap_includes as $file ) {
  $filepath = locate_template( 'inc' . $file );
  if ( ! $filepath ) {
    trigger_error( sprintf( 'Error locating /inc%s for inclusion', $file ), E_USER_ERROR );
  }
  require_once $filepath;
}

function isJSON($string) {
  json_decode($string);
  return (json_last_error() == JSON_ERROR_NONE);
}

function loadedImage( $img, $classes = array('img-fluid') ) {
  if ( function_exists('get_field') ) {

    $wpContentDIR = WP_CONTENT_DIR;
    $wpContentURL = WP_CONTENT_URL;
    $siteURL = get_bloginfo('url');
    $siteBaseURL = home_url();
    $siteBaseURL = preg_replace('/(^https\:\/\/|^http\:\/\/|^\/\/)/i', '', $siteBaseURL);
    $siteBaseURL = preg_replace('/(\/wp-content$|\/wp-content\/$)/i', '', $siteBaseURL);
    $siteBaseDIR = preg_replace('/(\/wp-content$|\/wp-content\/$)/i', '', $wpContentDIR);
    $svgClasses = "";

    if ( is_array( $img ) && array_key_exists( 'url', $img ) ) {
      $url = $img['url'];
      if ( is_array( $img ) && array_key_exists( 'subtype', $img ) ) {
        if ( $img['subtype'] === 'svg' || $img['subtype'] === 'svg+xml' ) {
          $url = $img['url'];
          if ( preg_match("/(^http\:\/\/|^https\:\/\/|^\/\/)$siteBaseURL\//i", $url ) ) {
            $svgPath = preg_replace("/(^http\:\/\/|^https\:\/\/|^\/\/)$siteBaseURL\//i", $siteBaseDIR . '/', $url);
            if ( file_exists($svgPath) ) {
              $svgContents = file_get_contents($svgPath);
              if ( $classes && is_array($classes) ) {
                foreach ( $classes as $class ) {
                  $svgClasses .= $class . ' ';
                }
              }
              if ( preg_match("/\<svg(.*) class=\"(.*)\"(.*)>/im", $svgContents ) ) {
                $svgContents = preg_replace("/\<svg(.*) class=\"([\w\-\d\s\_]*)\"(.*)>/im", "<svg$1 class=\"$2 $svgClasses\"$3>", $svgContents);
                echo $svgContents;
              } else {
                $svgContents = preg_replace("/\<svg /im", "<svg class=\"$svgClasses\" ", $svgContents);
                echo $svgContents;
              }
            }
          } else {
            $svgPath = preg_replace("/(^\/)/i", $siteBaseDIR . '/', $url);
            if ( file_exists($svgPath) ) {
              $svgContents = file_get_contents($svgPath);
              if ( $classes && is_array($classes) ) {
                foreach ( $classes as $class ) {
                  $svgClasses .= $class . ' ';
                }
              }
              if ( preg_match("/\<svg(.*) class=\"(.*)\"(.*)>/im", $svgContents ) ) {
                $svgContents = preg_replace("/\<svg(.*) class=\"([\w\-\d\s\_]*)\"(.*)>/im", "<svg$1 class=\"$2 $svgClasses\"$3>", $svgContents);
                echo $svgContents;
              } else {
                $svgContents = preg_replace("/\<svg /im", "<svg class=\"$svgClasses\" ", $svgContents);
                echo $svgContents;
              }
            }
          }
        }
      } else {
        $imgHTML = '<img src="' . $url . '" ';
        if ( $classes && is_array($classes) ) {
          $imgHTML .= 'class="';
          foreach ( $classes as $class ) {
            $imgHTML .= $class . ' ';
          }
          $imgHTML .= '"';
        }
        $imgHTML .= ' />';
        echo $imgHTML;
      }
    }
  }
  if ( is_string($img) && strlen($img) > 1 ) {
    if ( preg_match("/(^http\:\/\/|^https\:\/\/|^\/\/)([\/\.\w\s\d\-])*(\.svg$)/i", $img ) ) {
      if ( preg_match("/(^http\:\/\/|^https\:\/\/|^\/\/)$siteBaseURL\//i", $img ) ) {
        $svgPath = preg_replace("/(^http\:\/\/|^https\:\/\/|^\/\/)/i", '', $img);
        $svgPath = preg_replace("/($siteBaseURL\/)/i", $siteBaseDIR . '/', $svgPath);
        if ( file_exists($svgPath) && strlen(file_get_contents($svgPath)) > 1 ) {
          $svgContents = file_get_contents($svgPath);
          if ( $classes && is_array($classes) ) {
            foreach ( $classes as $class ) {
              $svgClasses .= $class . ' ';
            }
          }
          if ( preg_match("/\<svg(.*) class=\"(.*)\"(.*)>/im", $svgContents ) ) {
            $svgContents = preg_replace("/\<svg(.*) class=\"([\w\-\d\s\_]*)\"(.*)>/im", "<svg$1 class=\"$2 $svgClasses\"$3>", $svgContents);
            echo $svgContents;
          } else {
            $svgContents = preg_replace("/\<svg /im", "<svg class=\"$svgClasses\" ", $svgContents);
            echo $svgContents;
          }
        }
      }
      else {
        if ( file_exists($svgPath) && strlen(file_get_contents($svgPath)) > 1 ) {
          echo file_get_contents($svgPath);
        } else {
          $imgHTML = '<img src="' . $img . '" ';
          if ( $classes && is_array($classes) ) {
            $imgHTML .= 'class="';
            foreach ( $classes as $class ) {
              $imgHTML .= $class . ' ';
            }
            $imgHTML .= '"';
          }
          $imgHTML .= ' />';
          echo $imgHTML;
        }
      }
    }
    else if ( preg_match("/(^\/)([\/\.\w\s\d\-])*(\.svg$)/i", $img ) ) {
      $svgPath = preg_replace("/(^\/)/i", $siteBaseDIR . '/', $img);
      if ( file_exists($svgPath) && strlen(file_get_contents($svgPath)) > 1 ) {
        $svgContents = file_get_contents($svgPath);
        if ( $classes && is_array($classes) ) {
          foreach ( $classes as $class ) {
            $svgClasses .= $class . ' ';
          }
        }
        if ( preg_match("/\<svg(.*) class=\"(.*)\"(.*)>/im", $svgContents ) ) {
          $svgContents = preg_replace("/\<svg(.*) class=\"([\w\-\d\s\_]*)\"(.*)>/im", "<svg$1 class=\"$2 $svgClasses\"$3>", $svgContents);
          echo $svgContents;
        } else {
          $svgContents = preg_replace("/\<svg /im", "<svg class=\"$svgClasses\" ", $svgContents);
          echo $svgContents;
        }
      }
    }
    else if ( preg_match("/(^http\:\/\/|^https\:\/\/|^\/\/).*(\.jpg$|\.jpeg$|\.gif$|\.png$|\.webp$)/i", $img ) ) {
      $imgHTML = '<img src="' . $img . '" ';
      if ( $classes && is_array($classes) ) {
        $imgHTML .= 'class="';
        foreach ( $classes as $class ) {
          $imgHTML .= $class . ' ';
        }
        $imgHTML .= '"';
      }
      $imgHTML .= ' />';
      echo $imgHTML;
    } 
    else if ( preg_match("/(^\/wp-content)/i", $img ) ) {
      $img = preg_replace("/(^\/wp-content\/)/i", '//' . $siteBaseURL . '/wp-content/', $img);
      $imgHTML = '<img src="' . $img . '" ';
      if ( $classes && is_array($classes) ) {
        $imgHTML .= 'class="';
        foreach ( $classes as $class ) {
          $imgHTML .= $class . ' ';
        }
        $imgHTML .= '"';
      }
      $imgHTML .= ' />';
      echo $imgHTML;
    }
    else {
      $imgHTML = '<img src="' . $img . '" ';
      if ( $classes && is_array($classes) ) {
        $imgHTML .= 'class="';
        foreach ( $classes as $class ) {
          $imgHTML .= $class . ' ';
        }
        $imgHTML .= '"';
      }
      $imgHTML .= ' />';
      echo $imgHTML;
      echo '<h1 class="debug-pre" style="display: none;">FINAL CONDITION WARNING!</h1>';
    }
  }
} 



function featured_insight_query( $query ) {

  if( $query->is_main_query() && !is_admin() && is_post_type_archive( 'insights' ) ) {
    if ( $query->get('meta_query') ) {
      $meta_query = $query->get('meta_query');
    } else {
      $meta_query = array();
    }
    $meta_query[] = array(
        'key' => 'featured_insight', 
        'value' => '1',
        'compare' => '!='
    );
    $query->set('meta_query', $meta_query);
    $query->set( 'posts_per_page', '-1' );
  }
}

add_action( 'pre_get_posts', 'featured_insight_query' );

add_filter( 'rest_post_collection_params', 'apartments_change_post_per_page', 10, 1 );

function apartments_change_post_per_page( $params ) {
    if ( isset( $params['per_page'] ) ) {
        $params['per_page']['maximum'] = 200;
    }

    return $params;
}

